---
title:  "April 5th 12021 HE: Words of the day"
date:   2021-04-05 10:00:00 +0200
categories: ["wotd"]
---

## Interoception

> Interoception is the perception of sensations from inside the body and
includes the perception of physical sensations related to internal organ
function such as heart beat, respiration, satiety, as well as the autonomic
nervous system activity related to emotions.

Perception of bodily functions and state translates into emotions.

## Degeneracy (biology)

> The ability of elements that are structurally different to perform the same
function or yield the same output.

Many to one.

## Emergence

> In philosophy, systems theory, science, and art, emergence occurs when an
entity is observed to have properties its parts do not have on their own,
properties or behaviors which emerge only when the parts interact in a wider
whole.

The whole is greater than the sum of its parts.

## Antinomy

> A contradiction between two beliefs or conclusions that are in themselves
reasonable.

A paradox.
