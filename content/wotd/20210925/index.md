---
title:  "September 25th 12021 HE: Words of the day"
date: 2021-09-25T17:50:00+02:00
categories: ["wotd"]
---

## Epistemology

> Epistemology is the branch of philosophy concerned with knowledge.

Study of knowledge.

## Nociception

> Nociception is th sensory nervous system's process of encoding noxious
stimuli. In nociception, intense chemical, mechanical, or thermal stimulation
of sensory nerve cells called nociceptors produces a signal that travels along
a chain of nerve fibers via the spinal cord to the brain.

Perception of pain translates into emotion.

## Proprioception

> Proprioception (a.k.a. kinaesthesia / kinesthesia) is the sense of
self-movement and body position.

Spatial awareness and sense of one's own body.
