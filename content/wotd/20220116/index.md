---
title:  "January 16 12022 HE: Words of the day"
date: 2022-01-16T14:16:49+01:00
categories: ["wotd"]
---

## Confabulation (Psychology)

> Confabulation is a memory error defined as the production of fabricated,
distorted, or misinterpreted memories about oneself or the world. People who
confabulate present incorrect memories ranging from "subtle alterations to
bizarre fabrications", and are generally very confident about their
recollections, despite contradictory evidence.

Remembering things wrong with the utmost confidence.

## Contractualism (Ethical Theory by T.M. Scanlon)

> An act is wrong if its performance under the circumstances would be disallowed
by any set of principles for the general regulation of behaviour that no one
could reasonably reject as a basis for informed, unforced, general agreement.

The intentions matter as much as the consequences.

## Ontology

> Ontology is [...] the science of being.

The nature of things.

