---
title: "Zettelkasten"
date: 2022-01-22T20:30:33+01:00
draft: true
---

I need to write more about this. First I need to see how well it works.

It seems to be helping my writing and text composition a bit, but it's too soon
to tell. It could well be that it's only by virtue of making me think more
about writing and got me a bit more "daring" as well.

Anyway, once I have more data I will have more to write about here...
