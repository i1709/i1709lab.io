---
title:  "On Hammock Driven Development"
date:   2021-01-04 20:00:00 +0100
tags: ["learning"]
---

# Hammock Driven Development

[From Rich Hickey's talk.](https://www.youtube.com/watch?v=f84n5oFoZBc)

## Analysis & Design

It matters, as the **first** step of software development.

## Goal of software development

Solving problems.

## Practice

Makes perfect.

## The Problem

State it:

  * Aloud
  * Write it down

Understand it:

  * Facts
  * Context
  * Constraints

What do you know?

What **don't** you know?

Search for related problems.

## More input leads to better output

Read on and around the subject.

Critically study other solutions.

## Focus

To feed the *background* mind, one needs to **focus** on the problem.

## Sleep

Without **sleep**, the *background* mind cannot function properly.

## Capture

Any output from the *background* mind needs to be captured.

Even if it involves another problem you might have been working on.

## Be patient

It may take time.

Focus on one main problem a day, but you can work on multiple problems on
different days, while waiting for the output, if all leads are exhausted.

## Failure

You will be wrong. It's okay. Just start again.
