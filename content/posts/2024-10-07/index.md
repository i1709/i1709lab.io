---
title: "2024-10-07 - Last post...on this blog!"
date: 2023-10-07T19:00:00+02:00
---

## Shutting this place down

Want to learn more? [Read it on my new
blog](https://blog.lovsund.eu/posts/goodbye_and_hello/)
