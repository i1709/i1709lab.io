---
title:  "Hello World!"
date:   2020-09-27 22:00:00 +0200
---
```bash
echo "Hello, World!"
#=> prints 'Hello, World!' to STDOUT.
```

Thus spoke a random bash script.

