---
title:  "On indolence"
date:   2020-10-05 20:00:00 +0200
tags: ["opinion"]
---

Excerpt from Wikipedia:
> Laziness (also known as indolence) is disinclination to activity or exertion despite having the ability to act or to exert oneself

And that is perfectly fine. :)
