---
title:  "Code - Inheritance vs Composition"
date:   2021-01-10 14:00:00 +0100
tags: ["learning"]
---

# Inheritance VS Composition

## Preamble

Source code that is worked upon is living code. Yesterday's correct solution can
be in need of refactoring tomorrow. That is both normal and healthy, and such
changes should be actively encouraged.

Therefore, unless the software's design enforces strict limitations, a
developer should feel comfortable switching between inheritance and composition
within the same module, as the circumstances require.

## Inheritance

Object Oriented Programming (OOP) is an effective programming model to
encapsulate data structures and logic inherent to the handling of that data.

Inheritance further enables localised divergence in behaviour to be formalised
in a more concise fashion than repeated use of conditionals (i.e.
Programming-by-difference). Polymorphism (i.e. overriden parent methods), is
especially powerful in that area.

### The case of multiple inheritance (author's opinion)

Avoid multiple inheritance at all costs, as it's too complex to be reliable. If
you're stuck with it, then be prepared to know the class hierarchy and spend
time finding where everything is coming from.

## Composition

Use composition to package code into modules that are used in many different
unrelated places and situations.

This can help decrease complexity from an inherited class structure by
extracting oft-repeated concepts into their own objects.

An easy illustration to this would be the introduction of a separate "type"
object to a class structure, which would be referenced in the main class's
"type" field. Even a basic implementation of a "type" object can see immediate
payoff by leveraging the language's type system (i.e. the `instanceof` operator
in `java`, `javascript` or `php`, among others).

## When to use one or the other

This is entirely up to your own judgement. A good rule of thumb is to facilitate
comprehension; clarify or explicit intent behind the code.
