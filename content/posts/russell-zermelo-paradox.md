---
title: "Russell-Zermelo Paradox"
date: 2021-04-05T11:50:00+02:00
---


Pre-ZFC, Cantorian set theory:

> According to naive set theory, any definable collection is a set. Let R be
the set of all sets that are not members of themselves. If R is not a member of
itself, then its definition dictates that it must contain itself, and if it
contains itself, then it contradicts its own definition as the set of all sets
that are not members of themselves. This contradiction is Russell's paradox.

{{< svg "russel_zermelo_paradox.svg" >}}

