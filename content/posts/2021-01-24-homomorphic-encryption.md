---
title:  "Homomorphic Encryption"
date:   2021-01-24 11:00:00 +0100
tags: ["learning"]
---

This is a short post on the subject of homomorphic encryption.

# What is a homomorphic encryption?

## What is encryption?

Encryption is a way of transforming (i.e. encoding) information (i.e. data), in
order to keep that data secret from certain parties.

In computer science, this *usually* mean encoding plain text data into
encrypted data, that can only be made readable again on condition of knowing
how to reverse the process.

## What does homomorphic mean?

Adjective of homomorphism. From the greek "same shape" or "same form", a
homomorphism implies the preservation of structure mapping between two similar
structures.

## So, Homomorphic Encryption is...

An encryption technique that preserves the result of an operation applied on
encrypted data as compared to the same operation applied on the same
un-encrypted data.

This enables the transformation of encrypted data without prior decryption,
thus preserving secrecy while allowing changes.
