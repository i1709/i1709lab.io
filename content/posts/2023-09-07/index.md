---
title: "2023-09-07 - What's up?"
date: 2023-09-07T19:00:00+02:00
---

I haven't written anything in a while. Why? Same story as always,
slight variation.

I have bigger responsibilities these days, on top of a full time
job. Sleep is shorter and often interrupted. Which means that I only
have a very limited amount of time and energy, each day, to fool
around in front of my PC.

This all came to a head a few weeks ago when stress symptoms started
to show up. I really can't afford that kind of crap right now, for
other people's sake, so I've committed to severly reduce the scope of
my hobby learning. And reintroduce a dose of entertainment.

This does not mean that I'm not learning anything new, but far fewer
topics and at a reduced pace, without any strong commitment to feed
this blog.

Until next time! :)
