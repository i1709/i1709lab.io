---
title: "The Phoenix Project (Kim, Behr, Spafford)"
date: 2022-11-15T20:00:00+01:00
tags: ["opinion"]
categories: ["books"]
---

[This book](https://www.goodreads.com/book/show/17255186-the-phoenix-project)
turned out to be a surprisingly riveting read, considering the writing style
itself is rather so-so.

Some parts even had me feeling genuinely stressed and angry, as I imagine the
main character would have been.

The effect is most likely due to the fact that I could relate with the
subject matter: chaos and waste within IT departments and projects.

Why would I inflict myself such torment? I often heard this book mentioned in
combination with [Accelerate]({{< ref "/books/accelerate/index" >}}), so I read
both. I find that they complement each other quite well.

Ultimately I am hoping that the insights I have gained will help me in my daily
work.
