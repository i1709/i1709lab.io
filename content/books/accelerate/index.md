---
title: "Accelerate (Forsgren, Humble, Kim)"
date: 2022-11-05T18:00:00+01:00
tags: ["opinion"]
categories: ["books"]
---

[The book](https://www.goodreads.com/book/show/35747076-accelerate)

There's a ton of buzzword mumbo jumbo in my profession. Often built around the
kernel of a good idea but ending up mostly as corrupt, entropy producing group
practices.

Some organizations are robust and creative enough to filter out the bad and
make the best out of the good.

Yet too often things are done a certain way because of..._tradition_. Attempts
are made, but inertia endures.

This book is not a magic recipe to transform a dysfunctional organization into
and functional one.

But it provides **evidence** based ways of improvements. And that is the reason
why I would recommend it to folks in my industry.
