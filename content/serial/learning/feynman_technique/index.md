---
title: "Learning - The Feynman Technique"
date:  2023-06-13 16:00:00 +0200
---

## The Learning Loop

If you can teach it well, you understand it well:

{{< svg "feynman_technique.svg" >}}

