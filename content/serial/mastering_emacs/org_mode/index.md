---
title: "Mastering Emacs - Org mode"
date:  2023-06-04 13:00:00 +0200
---

[Previous post - Remote editing with TRAMP]({{< ref "/serial/mastering_emacs/tramp" >}})

## This mode...again

Years ago, I was in the habit of using Emacs only for Org mode. Mostly
to maintain TODO lists and similar small documents I would often
export to HTML.

It is an _immensely_ powerful tool. Just open it's `info` page. It
does **a lot**. It deserves its own exploratory serial on this blog.

That's not what I want to do here. Right now I'm coming back at it in
an attempt to leverage its _organisational_ features to get better
organised myself. While using Emacs. I'll actually write something
about that soonish.

So this post will mostly focus on a personal selection of core
features I want to quickly get to using on a daily basis.

## Let's go through the manual

The first § of the summary:

> Org is a mode for keeping notes, maintaining TODO lists, and project
planning with a fast and effective plain-text markup language.  It
also is an authoring system with unique support for literate
programming and reproducible research.

As explained earlier, I'll focus on the first part:

> Org is a mode for keeping notes, maintaining TODO lists, and project
planning with a fast and effective plain-text markup language.

Later on:

> Org [..] makes it possible to keep the content of large files well
structured.  Visibility cycling and structure editing help to work
with the tree.  Tables are easily created with a built-in table
editor.  Plain text URL-like links connect to websites [...] and any
files related to the projects.

The few bits I cut by the end of the § relate to links to services no
longer used or accessed in a manner relevant to my use cases.

Moving on:

> Org can be used to implement many different project planning
schemes, such as David Allen’s GTD system.

This is good. I'll come back to GTD in a future post (same as alluded
previously).

Next:

> Org keeps simple things simple. [...] Complexity is not imposed, but
a large amount of functionality is available when needed.  Org is a
toolbox.  Many users actually run only a—very personal—fraction of
Org’s capabilities, and know that there is more whenever they need it.

Good.

More:

> All of this is achieved with strictly plain text files, the most
portable and future-proof file format.

Very true.

Continuing:

> For a better experience, the three Org commands ‘org-store-link’,
‘org-capture’ and ‘org-agenda’ ought to be accessible anywhere in
Emacs, not just in Org buffers. To that effect, you need to bind them
to globally available keys.

```elisp
(global-set-key (kbd "C-c l") #'org-store-link)
(global-set-key (kbd "C-c a") #'org-agenda)
(global-set-key (kbd "C-c c") #'org-capture)
```

> Files with the ‘.org’ extension use Org mode by default. To turn on
Org mode in a file that does not have the extension ‘.org’, make the
first line of a file look like this:

```orgmode
MY PROJECTS    -*- mode: org; -*-
```

### Headlines

```orgmode
* Top level headline
** Second level
*** Third level
    some text
*** Third level
    more text
* Another top level headline
```

### Cycling

Visibility cycling is bound to the `<TAB>` key, `S-<TAB>` for global
cycling

Starting visibility is configurable through `org-startup-folded` or on
a per-file basis:

```orgmode
#+STARTUP: overview
#+STARTUP: content
#+STARTUP: showall
#+STARTUP: show2levels
#+STARTUP: show3levels
#+STARTUP: show4levels
#+STARTUP: show5levels
#+STARTUP: showeverything
```

It can be re-applied with `org-set-startup-visibility`.

Context can be revealed with `org-reveal`.

The `info` manual's page `Global and local cycling` contains more
information.

### Beware invisible edits

Set `org-catch-invisible-edits` to non-`nil`. The option's docstring
has more info.

### Motion

I'll copy the `info` manual page:

```info

‘C-c C-n’ (‘org-next-visible-heading’)
     Next heading.

‘C-c C-p’ (‘org-previous-visible-heading’)
     Previous heading.

‘C-c C-f’ (‘org-forward-heading-same-level’)
     Next heading same level.

‘C-c C-b’ (‘org-backward-heading-same-level’)
     Previous heading same level.

‘C-c C-u’ (‘outline-up-heading’)
     Backward to higher level heading.

‘C-c C-j’ (‘org-goto’)
     Jump to a different place without changing the current outline
     visibility.  Shows the document structure in a temporary buffer,
     where you can use the following keys to find your destination:

     ‘<TAB>’                Cycle visibility.
     ‘<DOWN>’ / ‘<UP>’      Next/previous visible headline.
     ‘<RET>’                Select this location.
     ‘/’                    Do a Sparse-tree search

     The following keys work if you turn off ‘org-goto-auto-isearch’

     ‘n’ / ‘p’              Next/previous visible headline.
     ‘f’ / ‘b’              Next/previous headline same level.
     ‘u’                    One level up.
     ‘0’ ... ‘9’            Digit argument.
     ‘q’                    Quit.

     See also the variable ‘org-goto-interface’.
```

### Structure editing

There are combinations of `C-`, `M-` and `S-` with `<RET>`
available. Try them out.

Promotions / Demotions with combinations of `M-`, `S-` and directions.

There exists `org-mark-subtree`, `org-cut-subtree`,
`org-copy-subtree` and `org-paste-subtree`.

`C-y` triggers `org-yank`.

You can sort same-level entries with `C-c ^`.

You can turn a normal line or plain list item into a headline with
`C-c *`.

### Sparse Trees

Keep as much of the document folded as possible but what you are
after: `C-c / r` will prompt for a regexp and shows a sparse tree with
all matches. Use `M-g n` or `p` to navigate.

### Lists

Ordered and unordered. Varying indentation level. Can be cycled and
edited on like the document structure. Can be sorted. Can be turned
into heading.

### Drawers

For information you do not want to see. Cannot contain headlines or
other drawers.

```orgmode
* headline
Some text.
:MYDRAWER:
Note on previous text.
:END:
More text.
```

Use `org-insert-drawer` to do it more easily.

### Hyperlinks

Link formats:

```orgmode
[[LINK][DESCRIPTION]]
[[LINK]]
```

Follow links with `C-c C-o`.

Use a combination of `org-store-link` and `org-insert-link` / `C-c
C-l` (NB this also edits existing links) to automatically store links
in the right format and insert them where needed.

Use `C-u C-c C-c` to link to files.

Use `C-c %` and `C-c &` to respectively save positions and return to
them.

From the manual:

```info
‘C-c C-x C-n’ (‘org-next-link’)
‘C-c C-x C-p’ (‘org-previous-link’)
     Move forward/backward to the next link in the buffer.  At the limit
     of the buffer, the search fails once, and then wraps around.  The
     key bindings for this are really too long; you might want to bind
     this also to ‘M-n’ and ‘M-p’.

          (with-eval-after-load 'org
            (define-key org-mode-map (kbd "M-n") #'org-next-link)
            (define-key org-mode-map (kbd "M-p") #'org-previous-link))
```

### TODO

TODO items are first class citizens in Org mode documents. Org mode
provides facilites to quickly overview scattered TODOs throughout a
document.

Either mark an item with `TODO` or use `C-c C-t` to rotate the tag at
a given position.

Show TODO items in a sparse tree with `C-c / t` /
`org-show-todo-tree`. Find a specific todo with `C-c / T`.

#### More states

By default only `TODO` and `DONE` states exist, but you can augment
these. Example:

```elisp
(setq org-todo-keywords
      '((sequence "TODO" "FEEDBACK" "VERIFY" "|" "DONE" "DELEGATED")))
```

You could also define multiple sub-sequences, with fast access
keys. Example:

```elisp
(setq org-todo-keywords
      '((sequence "TODO(t)" "|" "DONE(d)")
        (sequence "REPORT(r)" "BUG(b)" "KNOWNCAUSE(k)" "|" "FIXED(f)")
        (sequence "|" "CANCELED(c)")))
```

You could then jump between sub-sequences, when cycling, with `C-S-`
and left / right arrow keys. `S-` and left / right arrow keys walks
through all sub-sequences.

This can be defined on a per-file basis, by adding these lines:

```orgmode
#+TODO: TODO(t) | DONE(d)
#+TODO: REPORT(r) BUG(b) KNOWNCAUSE(k) | FIXED(f)
#+TODO: | CANCELED(c)
```

#### Dependencies

Dependencies can be enforced for TODOs:
`org-enforce-todo-dependencies`.

`ORDERED` properties (`C-c C-x o` to toggle
`org-toggle-ordered-property`) in a `PROPERTIES` drawer enforces
dependencies from top to bottom of TODO items. It has an opposite
`NOBLOCKING` property.

#### Logging

By default, a `DONE` item is timestamped.

```elisp
(setq org-log-done 'time)
```

But you can add a note instead.

```elisp
(setq org-log-done 'note)
```

```orgmode
#+STARTUP: lognotedone
```

#### Progress tracking

You can track progress on broken down tasks by adding a special cookie
`[/]` or `[%]` in the headline. It is updated when subtask state
changes or manually with `C-c C-c`. This works by default for direct
children. To include the entire subtree, use the `COOKIE_DATA`
property with the word `recursive` into its value.

TODOs can be substituted by `[ ]` checkboxes. They use the same
progress cookies and are more geared toward tracking atomic tasks.

[Next post - Modularize init.el]({{< relref "/serial/mastering_emacs/modularize_init" >}})
