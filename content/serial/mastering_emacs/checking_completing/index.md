---
title: "Mastering Emacs - Checking and Completing"
date:  2023-05-26 21:00:00 +0200
---

This is the first post in a serial dedicated to harnessing the power
of Emacs.

## Spell-check

To toggle spell checking when text editing on the current buffer, type
`M-x flyspell-mode`.

If you want to spell-check code strings and comments, use `M-x
flyspell-prog-mode`.

You could setup in your `init.el` file a user defined shortcut to
toggle it:

```elisp
(global-set-key (kbd "C-c f") 'flyspell-mode)
```

Emacs has two main spell-checkers: `ispell` and `aspell`. You can
check which one your running Emacs distribution is using by typing
`M-: ispell-program-name`.

Once you know, install the corresponding dictionary packages. E.g. for
`Debian` based distributions with Emacs using `aspell`:

```bash
apt install aspell-fr aspell-en aspell-sv
```

To change the dictionary used by the spell-checker, type `M-x
ispell-change-dictionary`.

## Code syntax check

I was starting to dig into `flymake` when a bearded Emacs veteran
friend of mine pointed me towards
[flycheck](https://www.flycheck.org/en/latest/user/flycheck-versus-flymake.html).

It seems like `flycheck` supports many more languages than `flymake`,
as a consequence of the FSF demanding assignments of copyrights to
themselves when contributing to `flymake`.

In any case, `flycheck` can be installed through MELPA.

It will pretty much work right out of the box, see the
[quickstart](https://www.flycheck.org/en/latest/user/quickstart.html)
instructions if necessary.

Syntax checking programs will need to be installed on your system for
`flycheck` to use them, you can see if the opened buffer's checkers
are available with `C-c ! v`.

Some more shortcuts:
```
    C-c ! n     next error
    C-c ! p     previous error
    C-c ! l     opens a list with all errors in current buffer
```
## Auto completion

Use `M-TAB` or `C-M-i` to complete the current word or syntax.

For code, the default behaviour of completion is to use tags. You need
to use Semantic for parser aided completion.

Semantic is a suite of Emacs libraries and utilities for parsing
source code using language parsers. You can enable it by typing `M-x
semantic-mode` or adding `(semantic-mode 1)` to your `init.el` file.

Semantic parses the files you have visited.

Some shortcuts:
```
    C-c j       - jump to given function definition in current file
    C-c J       - jump to given function definition in parsed file
    C-c <SPC>   - display list of completions for symbol at point
    C-c l       - display list of completions for symbol at point
                  in another window
```

## Wait...that's it?

No. [lsp-mode](https://emacs-lsp.github.io/lsp-mode/) offers modern
IDE capabilities, with integrations to several other plugins enhancing
Emacs.

But we'll explore that some other time.

[Next post - Remote editing with TRAMP]({{< ref "/serial/mastering_emacs/tramp" >}})
