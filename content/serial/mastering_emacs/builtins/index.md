---
title: "Mastering Emacs - Built Ins"
date:  2023-06-16 21:00:00 +0200
---

[Previous post - Modularize init.el]({{< ref "/serial/mastering_emacs/modularize_init" >}})

## Packaged with Emacs

Here I'll quickly go through a few built in (distributed with Emacs)
extensions: `viper` for Vi inside Emacs, `project` for project related
functions and `image-dired` to use `dired` as a serious file explorer
alternative.

### viper

Do you want to bask in Heresy™ and taste the sweetness of Vi Insert
and Command modes inside Emacs? `M-x viper-mode`. After answering a
few questions, that will setup your `viper` configuration file, you
will be ON.

Use `C-z` to switch between `<V>` and `<E>` modes. `M-x viper-go-away`
will turn off `viper-mode`.

I suggest setting up a user key binding for `M-x toggle-viper-mode`:

```elisp
(global-set-key (kbd "C-c v") #'toggle-viper-mode)
```

### project

This is the right extension for those who want to easily jump between
projects, search files, directories or specific text and run shell
commands in those projects. Version controlled root directories are
interpreted as project root folders.

From the manual:

```info
‘C-x p f’
     Visit a file that belongs to the current project
     (‘project-find-file’).
‘C-x p g’
     Find matches for a regexp in all files that belong to the current
     project (‘project-find-regexp’).
‘M-x project-search’
     Interactively search for regexp matches in all files that belong to
     the current project.
‘C-x p r’
     Perform query-replace for a regexp in all files that belong to the
     current project (‘project-query-replace-regexp’).
‘C-x p d’
     Run Dired in the current project’s root directory
     (‘project-dired’).
‘C-x p v’
     Run ‘vc-dir’ in the current project’s root directory
     (‘project-vc-dir’).
‘C-x p s’
     Start an inferior shell in the current project’s root directory
     (‘project-shell’).
‘C-x p e’
     Start Eshell in the current project’s root directory
     (‘project-eshell’).
‘C-x p c’
     Run compilation in the current project’s root directory
     (‘project-compile’).
‘C-x p !’
     Run shell command in the current project’s root directory
     (‘project-shell-command’).
‘C-x p &’
     Run shell command asynchronously in the current project’s root
     directory (‘project-async-shell-command’).

'M-x fileloop-continue'
     Continue 'M-x project-search' & 'C-x p r'

‘C-x p b’
     Switch to another buffer belonging to the current project
     (‘project-switch-to-buffer’).
‘C-x p k’
     Kill all live buffers that belong to the current project
     (‘project-kill-buffers’).

‘C-x p p’
     Run an Emacs command for another project
     (‘project-switch-project’).

‘M-x project-forget-project’
     Remove a known project from the ‘project-list-file’.
```

### image-dired

You want to visualize images as thumbnails inside Emacs while browsing
your directories with `dired`? Use `M-x image-dired`. It is as simple
as that. And very neat.
