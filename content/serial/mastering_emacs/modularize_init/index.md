---
title: "Mastering Emacs - Modularize init.el"
date:  2023-06-11 09:00:00 +0200
---

[Previous post - Org mode]({{< ref "/serial/mastering_emacs/org_mode" >}})

## Modularize `init.el`

Instead of maintaining one big `init.el` file, one can split it into several files that would be loaded during initialization. As a matter of fact, `init.el` should contain the bare minimum logic to make this setup work.

To achieve this, setup the path of your configuration files:

```elisp
(add-to-list `load-path "~/.emacs.d/custom")
```

Define the configuration files as a list of strings:

```elisp
(defvar addons
  '("ui.el"
    "bindings.el"
    "misc.el"))
```

Load the files:

```elisp
(dolist (x addons)
  (load x))
```

[Next post - Built Ins]({{< ref "/serial/mastering_emacs/builtins" >}})
