---
title: "Mastering Emacs - Remote editing with TRAMP"
date:  2023-06-01 20:00:00 +0200
---

[Previous post - Checking and Completing]({{< ref "/serial/mastering_emacs/checking_completing" >}})

## These acronyms...

Use TRAMP to remotely edit files through different protocols.

Since `ssh` is going to be the most likely remote access protocol to
be used, start with setting this up in Emacs' init file:

```elisp
(setq tramp-default-method "ssh")
```

Then open a file as you usually would (`C-x C-f`), with the difference
that you would prefix the absolute path with
`/method:user@host`. E.g.: `/ssh:worf@defiant:/home/worf/honour.md`.

NB: with `tramp-default-method` set, you can replace the `method` part
with `-`. When using `ssh`, if your `.ssh/config` file has the
necessary information, you can omit the `user@` part as well.

That all refines to: `/-:defiant:/home/worf/honour.md`.

## Editing with privileges

You can also use TRAMP to `sudo` edit files:
`/sudo::/etc/starfleet/tactical.conf`.

A `sudoedit` version exists as well, but it's localhost only and has
other limitations, for security reasons.

## Why not both?!

Let's cut straight to the chase:
`/-:defiant|sudo::/etc/module/cloaking.device`.

## Encountered issue

If the remote host's user uses a colour coded fancy default prompt,
TRAMP is going to hang as it won't recognize the shell prompt. Make
sure `tramp-terminal-type` with `M-x customize-variable` is set to
`dumb`, and modify your shell `rc` files accordingly.

As an example, when using `powerline`, your `.bashrc` file should be
modified like this:

```bash
if [ "$TERM" != "dumb" ]; then
    if [ -f "$(which powerline-daemon)" ]; then
        powerline-daemon -q
        # shellcheck disable=SC2034
        POWERLINE_BASH_CONTINUATION=1
        # shellcheck disable=SC2034
        POWERLINE_BASH_SELECT=1
        . /usr/share/powerline/bindings/bash/powerline.sh
    fi
fi
```

## Teaser

TRAMP also provides facilities for more remote actions (shell,
processes, etc), but let us explore that later on.

[Next post - Org mode]({{< ref "/serial/mastering_emacs/org_mode" >}})
