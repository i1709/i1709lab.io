---
title: "Desktop Tuning - Reading AwesomeWM's docs"
date: 2023-06-24T22:00:00+02:00
---

## After 10 years

How did I end up using this window manager for so long without
actually figuring out how it works? Rhetorical question...

## So here I am

And I'll leave myself some breadcrumbs along the way. Hopefully I may
as well spend enough time will `lua` that some might actually stick.

## My notes

Managed to customise something decent, starting from the default `rc.lua` and going for a solarized theme. Here's some documentation: 

- [Main docs](https://awesomewm.org/doc/api/documentation/07-my-first-awesome.md.html#)
- [Theme docs](https://awesomewm.org/apidoc/documentation/06-appearance.md.html)
- Test with `Xephyr :5 & sleep 1 ; DISPLAY=:5 awesome`
- [Solarized colours](https://en.wikipedia.org/wiki/Solarized)
- Test configuration with `awesome -k`
- On Debian, logs found at : `$HOME/.xsession-errors`

[Next post - rofi]({{< ref "/serial/desktop_tuning/rofi" >}})
