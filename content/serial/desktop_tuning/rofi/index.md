---
title: "Desktop Tuning - rofi"
date: 2023-06-25T21:00:00+02:00
---

[Previous post - Reading AwesomeWM's docs]({{< ref "/serial/desktop_tuning/awesome_docs" >}})

## Functionality

According to the project's description:

> A window switcher, Application launcher and dmenu replacement.

What the hell does that mean?

Let's look at the manpage:

> A window switcher, application launcher, ssh dialog, dmenu
> replacement and more.

Huh. Okay...?

Read some more:

> rofi's main functionality is to assist in your workflow, allowing
> you to quickly switch between windows, start applications or log
> into a remote machine via ssh.

Alright that is starting to make more sense. Took a bit more effort to
get there than I would have liked.

### Window switcher

You can show and select all opened windows:

```bash
rofi -show window
```

Or only on the current desktop:

```bash
rofi -show windowcd
```

### Application launcher

Launch an application through `rofi`:

```bash
rofi -show run
```


### `ssh` dialog

Start a `ssh` connection based on your `.ssh/config`:

```bash
rofi -show ssh
```

### and more?

Key binding help:

```bash
rofi -show keys
```

Multiple modes, navigate with `S-←` / `S-→`:
```bash
rofi modi run,window,ssh -show run
```

Combine modes in the same view:
```bash
rofi -modi combi -show combi -combi-modi run,drun
```

## Appearance

This is about tuning, so modifying how `rofi` looks is important in
order for it to fit with the rest of the desktop's visual experience.

Depending on how you installed `rofi`, it may or may not be distributed with a set a themes.

You can browse them with:
```bash
rofi-theme-selector
```

And use one by inserting the following line inside your `config.rasi` file:
```
@theme "solarized_alternate"
```

It uses `css` as a style language. Which is nice from an accessibility point of view.

Read the
[documentation](https://github.com/davatorium/rofi/blob/next/doc/rofi-theme.5.markdown)
to learn more.
