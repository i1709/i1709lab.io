---
title: "(re)learning CS - Using packages in Emacs with MELPA"
date:  2023-04-20 16:40:00 +0200
---

[Previous post - Basic of C with Emacs]({{< ref "/serial/relearning_cs/emacs_c" >}})

This post will skip over Emacs' native package management and focus
instead of installing packages using the MELPA repository.

## [MELPA](https://melpa.org/#/) repository

Read the "[Getting started](https://melpa.org/#/getting-started)"
section of the MELPA website to get a fuller picture.

Add the following to you Emacs configuration file:

```elisp
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
```

Restart Emacs then update the package list with:

* `M-x package-list-packages` (type `U` to upgrade installed packages)

Start installing packages with:

* `M-x package-install`

[Next post - Scheming with Emacs]({{< ref "/serial/relearning_cs/emacs_scheme" >}})
