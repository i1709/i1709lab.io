---
title: "(re)learning CS - A little recursion"
date:  2023-05-23 17:00:00 +0200
---

[Previous post - Customizing Emacs]({{< ref "/serial/relearning_cs/emacs_custom" >}})

## [Definition]({{< ref "a_little_recursion" >}})

## No seriously though, what is it?

When talking about recursion in CS we usually mean recursive
functions: they are defined by making calls to themselves (with some
limit conditions to avoid running out of stack space).

## Recursive procedure

An easy way to play with recursion is to define a
[factorial](https://en.wikipedia.org/wiki/Factorial) function recursively:

```scheme
(define (factorial n)
  (if (< n 2)
      1
      (* n (factorial (- n 1)))))
```

Above is a straightforward implementation of a factorial function. If
you set aside the lispiness of the code snippet (e.g. parens and
prefix notation), it _should_ be easy to read.

### Recursive process

Can this function return all possible values of `n!`?

No: Stack space is
[limited](https://www.tfeb.org/fragments/2023/03/25/the-absurdity-of-stacks/),
and this implementation returns a value only after digging all the way
down to `1!`.

### Iterative process

A workaround is to use your compiler's tail recursive capabilities: by
writing your recursive function in a way that enables the compiler to
optimize the program not to consume the stack.

```scheme
(define (factorial n)
  (define (iter product counter)
    (if (> counter n)
        product
        (iter (* counter product)
              (+ counter 1))))
  (iter 1 1))
```

There's more lispiness going on here, but the core idea is that each
iteration computes a value that is passed along to the next one. The
last iteration returns the final value.

### Tree recursion

Traversing a tree offers a good candidate for implementing a recursive
procedure. A good example is [Fibonacci's
sequence](https://en.wikipedia.org/wiki/Fibonacci_sequence):

```scheme
(define (fibonacci n)
  (cond ((< n 1) 0)
        ((< n 3) 1)
        (else (+ (fibonacci (- n 1)) (fibonacci (- n 2))))))
```

An issue with this naive implementation is that the same value
**will** be computed several times, and that's inefficient.

This is solved by either writing an iterative solution, or rely on
compiler optimization
(i.e. [memoisation](https://en.wikipedia.org/wiki/Memoization)).

## Is recursion good?

Depends. Is the code more readable if defined recursively?  Will stack
limit be an issue? Is there a recursive implementation producing an
iterative process that preserves clarity?

## Is that all?

Most likely not. But it should cover the basics

[Next post - Expression Evaluation]({{< ref "/serial/relearning_cs/evaluating_expressions" >}})
