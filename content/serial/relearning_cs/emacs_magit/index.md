---
title: "(re)learning CS - Magic with Magit for Emacs"
date:  2023-04-23 15:00:00 +0200
---

[Previous post - Scheming with Emacs]({{< ref "/serial/relearning_cs/emacs_scheme" >}})

## Magic with Magit

### Installation

Install it using MELPA after refreshing the package list.

### Usage

Read the `info` manual `C-h i <RET> m Magit <Ret>`.

```
    C-x g       Interact with Magit

    n           Move down between sections
    p           Move up between sections
    h           Open main menu
    s           Stage
    u           Unstage
    g           Refresh from withing the Magit buffer
    c           Open the commit menu
    C-c C-c     Create the commit after writing the message
    P           Open the push menu
```

[Next post - Customizing Emacs]({{< ref "/serial/relearning_cs/emacs_custom" >}})
