---
title: "(re)learning CS - prelude"
date:  2023-04-04 21:00:00 +0200
---

## Background

Eighteen years ago I'd decided that I would not follow my brothers'
path into the IT industry. Computers were entertainment, not work.

Today I work as a system developer, after starting my career in QA and
slowly working my way into writing and maintaining software for a
living.

The sinuous path I took along the way has resulted in me having gaps
in certain areas of Computer Science. Those gaps have been like an
itch I've been wanting to scratch for years. I have started often and
stopped just as often.

I feel like it is time for me to start again. In order to keep me
motivated at the task longer than the previous times, I've decided to
combine this attempt with a few other projects I've always wanted to
get going.

### Mastering Emacs

Although I was first exposed to Emacs during my studies in physics, it
is Vim that became my text editor of choice once I really started
working my way into IT. Over the years I've approached Emacs multiple
times, and I've had the nagging feeling that mastering it would feel
rewarding in ways that mastering Neovim has not.

### Truly learning a functional programming language

I've played around with a bit of Haskell and I easily reach for
functional approaches when writing software in imperative languages
that permit it. But I've never actually had the formal training
required to fully master this mindset.

### Working closer to the Linux Kernel

I enjoy working close to the system and being aware of its inner
workings when developing software. At best, I could currently be
considered a knowledgeable superuser. My goal is to go a step further.

### More frequently writing blog posts

Writing is tough, and I'm not very good at it. But I'd like to
improve. No better way to do that than by writing more, I suppose...

## The Project

I am going to fill my gaps in Computer Science and acquire a deeper
knowledge of System Programming by:

* Deepening my knowledge of C and the C compiler
* Getting proficient in Rust
* Learning by writing programs that I would package and use
* Using Emacs as my main text editor for this endeavor
* Learn Emacs Lisp in order to truly master Emacs
* Customize Emacs to turn it into an IDE that suits my taste
* Blog about this journey frequently by hitting specific milestones
  frequently

I am curious to see where I'll end up along the way.

[Next post - Primer on Emacs]({{< ref "/serial/relearning_cs/emacs_primer" >}})
