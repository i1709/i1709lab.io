---
title: "(re)learning CS - Basics of C with Emacs"
date:  2023-04-15 21:00:00 +0200
---

[Previous post - Emacs basic help]({{< ref "/serial/relearning_cs/emacs_help" >}})

## Basics of working on a C project with Emacs

### Dependencies

On Debian, you may want to install a few extra packages:
* `make-doc`
* `gcc-doc`
* `glibc-doc`
* `gdb-doc`

### Program Source Code

Here's a little program that demonstrates some basic C coding
mechanics.

The standard `libc` documentation is readable through Emacs' info
mode, as well as `make`, `gcc` and `gdb` commands and syntax. Get to
`gdb` manual with `M-x info-display <RET> gdb <RET>`.

`main.c`:
```C
#include <stdio.h>
#include <time.h>

#include "mylib.h"

int main() {
    print_current_time();
    struct tm dob = {28, 15, 7, 23, 7-1, 113, 6, 1};
    print_time(&dob);
    return 0;
}
```

`mylib.h`:
```C
#ifndef MYLIB_H
#define MYLIB_H

void print_current_time();
void print_time(struct tm* pBrTime);

#endif
```

`mylib.c`:
```C
#include <stdio.h>
#include <time.h>

#include "mylib.h"

void print_time(struct tm* bTime) {
    printf("The input time is: %s\n", asctime(bTime));
}

void print_current_time() {
    time_t t = time(NULL);
    struct tm* currTime = localtime(&t);
    
    printf("The time is: %s\n", asctime(currTime));
}
```

Compile and run the program after opening a shell `M-x shell`:

```sh
gcc main.c mylib.c -o myprog
```

Or you can use this Makefile with `M-x compile`:

`Makefile`:
```make
CFLAGS = -g -std=c17 -Wall
objects = main.o mylib.o

testprog: main.o mylib.o target
	gcc -o target/testprog main.o mylib.o

mylib.o: mylib.c
	gcc -o mylib.o -c mylib.c

main.o: main.c
	gcc -o main.o -c main.c

target:
	mkdir target

clean:
	rm -f target/* $(objects)
```

### Cheatsheet

```
* SHELL
-------

   M-x shell        Start a shell in a new window
   M-! <shell cmd>  One-off shell command
   M-|              Run shell command on the region

* MOVEMENT
----------

   C-M-f    Move point forward to the matching symbol (e.g. '}')
   
* C Project
-----------

   M-x compile           Compile
   M-x rgrep             Search in the entire project
   M-x vc-               Basic version control commands
   C-x v                 Binding prefix for vc commands
   M-x gdb               Use the debugger
   
* Help
------

   info-lookup-symbol    Displays the definition of the symbol found 
                         in the manual
   info-display-manual   Displays an info buffer with the input
                         manual
   C-h F                 Find documentation for command
   C-h S                 Symbol help mode
```

[Next post - Managing Emacs packages]({{< ref "/serial/relearning_cs/emacs_packages" >}})
