---
title: "2023-05-22 Status Update - (re)learning CS"
date:  2023-05-22 20:30:00 +0200
---

A month without any new post. Where am I at?

## CS

### SICP

Going slowly through the book. Taking the time to think through the
material and the assignments. That has been a bit time consuming. I
have written down notes as material for more posts, but I _feel_ like
I have **not** gathered enough material to publish any of them...

Learning and writing Scheme is both fun and forcing me to think about
and write code in a different manner that I am used to.

### Discrete Mathematics

It's one of my ongoing regret in life to not have taken seriously
enough the field of Mathematics while in high school and at the Uni. I
most definitely could have done better considering both the enjoyment
and good grades I got once I applied myself, but back then I did not
see the point...

Now I'm picking up Uni entry level stuff again and taking the time to
think through the material and stretch my brain properly.

Definitely not enough publishable material yet.

## Emacs

I've setup Emacs as both MIT Scheme / Common Lisp IDE. I write with
Emacs as much as I can. Use `magit` as much as I can.

It remains fun.

## Writing

Not much in a month, up to this post. I need to up my game here.

## Some Perspective

Passively reading is easy. I am very good at it and I am
lazy.

Laziness is all about doing things you do not need to think about too
much, because you have integrated that activity so well that it is
merely a matter of executing a known task.

Deep learning requires effort. Higher education requires a sustained
amount of deep learning level effort. It is not just about breezing
through text. And the text is not as digestible as story telling. You
need to focus, read, think about what you just read, take notes,
doodle.

You need to do the suggested assignments.

Then you have to apply that knowledge outside the confines of the text
book to assess how much you have integrated and what remains
undigested.

Ultimately you need to teach it.

All of that I know. Practicing that activity is what this endeavour is
all about.

But old traps remain. I have identified four of them:

1. procrastination
2. distraction
3. obsessing about the big picture
4. the high of learning something new

The first two I know how to deal with.

The last two have been plaguing me for a month.

### The Big Picture

I am experienced enough to understand the alluring treachery of this
trap. It gives the impression that you are doing something of value
toward reaching the goal you have set for yourself by thinking about
the path that lays ahead.

But it usually turns into ever so slowly giving up through quietly
putting less and less time into your work until it joins the pile of
shelved projects.

It can be useful to look up from your task and look ahead, but such an
action should be constrained in time and space at various intervals
and should not interfere with what you are set on doing.

### The High of Learning New Stuff

Learning something new usually starts by picking the low hanging
fruits of chunked knowledge. They are tasty and rewarding and very
easily accessed. Once you get into the higher branches, that you have
to put in some work and the interval between each fruit is increasing,
the temptation of running to that the next tree and picking the low
hangers there is alluring.

After all, you were planning on getting to that tree anyway, right?

You inevitably repeat the process, until you have run out of easy
pickings, at which point going back to _any_ tree is a chore. All the
more harder the further away you have strayed from the first ones.

## The Way Ahead

I know the target. I should pick a task, work through it and move on
to the next. I should stay within capacity and preserve forward
momentum.