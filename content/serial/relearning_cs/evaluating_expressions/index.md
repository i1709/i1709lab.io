---
title: "(re)learning CS - Expression Evaluations"
date:  2023-05-23 17:15:00 +0200
---

[Previous post - A little recursion]({{< ref "/serial/relearning_cs/a_little_recursion" >}})

## What's this about?

Considering the following:

```scheme
(define (triple a)
  (+ a a a))

(define (square a)
  (* a a))
```

How would a compiler evaluate this expression:

```scheme
(square (triple 1))
```

In the case of Scheme, like this:

```scheme
(square (triple 1))
(square (+ 1 1 1))
(square 3)
(* 3 3)
9
```

It is called the applicative-order evaluation.

The evaluation will procede as follows: all internal expressions are
evaluated until they can no longer be reduced / have reached their
normal form, then is the lefternmost procedure evaluated.

One problem with this type of evaluation is that the resulting process
might not reach any exit condition. Let's look at an example:

```scheme
(define (countdown-to-zero n)
  (if (= n 0)
      0
      (countdown-to-zero (- n 1))))
```

Suppose the `if` above uses the applicative-order evaluation, what
would be the result?

The recursive expression would be continuously evaluated until the
Scheme implementation aborts, you run out of patience or the computer
is shutdown.

Thankfully, `if` is a special form: the evaluation will be in
normal-order.

Normal-order evaluation means that the lefternmost procedure is
evaluated first and so on until it can no longer be reduced. In our
example it leads to eventually returning 0, an not bothering
evaluating the recursive procedure.

If we re-use our first example, it would be evaluated as follows:

```scheme
(square (triple 1))
(* (triple 1) (triple 1))
(* (+ 1 1 1) (triple 1))
(* 3 (triple 1))
(* 3 (+ 1 1 1))
(* 3 3)
9
```

Programming languages do not use normal-order evaluation by default,
because it is not an efficient solution: identical expressions can be
evaluated multiple times, as seen above.

Most functional programming languages these days use lazy evaluation:
an expression is only evaluated when it is needed, and it's value
saved so that similar calls will be directly replaced with that value.
