---
title: "(re)learning CS - Emacs primer"
date:  2023-04-05 21:00:00 +0200
---

[Previous post - Prelude]({{< ref "/serial/relearning_cs/prelude" >}})

Here's a cheatsheet I made using Emacs based on the TUTORIAL
text. I left the legalese untouched at the end.

```
* GOOD TO KNOW
--------------

	C-x C-c	End Emacs session
	C-g 	To quit a partially entered command

* SCROLLING
-----------

	C-v	Move forward one screenful
	M-v	Move backward one screenful
	C-l	Clear screen and redisplay all the text,
		 moving the text around the cursor
		 to the center of the screen.

* BASIC CURSOR CONTROL
----------------------

	C-f	Move forward a character
	C-b	Move backward a character

	M-f	Move forward a word
	M-b	Move backward a word

	C-n	Move to next line
	C-p	Move to previous line

	C-a	Move to beginning of line
	C-e	Move to end of line

	M-a	Move back to beginning of sentence
	M-e	Move forward to end of sentence

	M-<	Move to beginning of file
	M->	Move to end of file

* WINDOWS
---------

	C-x 1	One window (i.e., kill all other windows)
	C-x 2 	Split the screen into two windows
	C-M-v 	Scroll the unfocused window
	C-x o 	Focus on the next window

* INSERTING AND DELETING
------------------------

	<DEL>        Delete the character just before the cursor
	C-d   	     Delete the next character after the cursor

	M-<DEL>      Kill the word immediately before the cursor
	M-d	     Kill the next word after the cursor

	C-k	     Kill from the cursor position to end of line
	M-k	     Kill to the end of the current sentence

	C-<SPC>	     Toggle mark to select a segment of text
	C-w	     Kill text between point and mark
	M-w	     Copy text between point and mark
	C-y	     Reinsert the last stretch of killed text
	M-y	     Yank from kill-ring

* UNDO
------

	C-/	Undo

* FILES
-------

	C-x C-f   Find a file
	C-x C-s   Save the file

* BUFFERS
---------

	C-x C-b   List buffers
	C-x s     Save some buffers to their files

* EXTENDING THE COMMAND SET
---------------------------

	C-x	Character eXtend.  Followed by one character.
	M-x	Named command eXtend.  Followed by a long name.
	C-x C-f		Find file
	C-x C-s		Save buffer to file
	C-x s		Save some buffers to their files
	C-x C-b		List buffers
	C-x b		Switch buffer
	C-x C-c		Quit Emacs
	C-x 1		Delete all but one window
	C-x u		Undo

* SEARCHING
-----------

	C-s	Forward search
	C-r	Backward search

* MULTIPLE FRAMES
------------------

	C-x 5 2	Open new frame
	C-x 5 0	Close current frame

* VISUAL
--------

        C-x C-+ Increase font size
        C-x C-- Decrease font size
        C-x C-0 Default font size

* GETTING MORE HELP
-------------------

To use the Help features, type the C-h character, and then a
character saying what kind of help you want.  If you are REALLY lost,
type C-h ? and Emacs will tell you what kinds of help it can give.
If you have typed C-h and decide you do not want any help, just
type C-g to cancel it.

The most basic HELP feature is C-h c.  Type C-h, the character c, and
a command character or sequence; then Emacs displays a very brief
description of the command.

To get more information about a command, use C-h k instead of C-h c.

   C-h x	Describe a command.  You type in the name of the
		command.
   C-h a	Command Apropos.  Type in a keyword and Emacs will list
		all the commands whose names contain that keyword.
		These commands can all be invoked with META-x.
		For some commands, Command Apropos will also list a
		sequence of one or more characters which runs the same
		command.
   C-h i	Read included Manuals (a.k.a. Info).  This command puts
		you into a special buffer called "*info*" where you
		can read manuals for the packages installed on your system.
		Type m emacs <Return> to read the Emacs manual.
		If you have never before used Info, type h and Emacs
		will take you on a guided tour of Info mode facilities.
		Once you are through with this tutorial, you should
		consult the Emacs Info manual as your primary documentation.


* MORE FEATURES
---------------

You can learn more about Emacs by reading its manual, either as a
printed book, or inside Emacs (use the Help menu or type C-h r).
Two features that you may like especially are completion, which saves
typing, and Dired, which simplifies file handling.

Dired enables you to list files in a directory (and optionally its
subdirectories), move around that list, visit, rename, delete and
otherwise operate on the files.  Dired is described in the Emacs
manual in the node called "Dired".

* COPYING
---------

This tutorial descends from a long line of Emacs tutorials
starting with the one written by Stuart Cracraft for the original Emacs.

This version of the tutorial is a part of GNU Emacs.  It is copyrighted
and comes with permission to distribute copies on certain conditions:

  Copyright (C) 1985, 1996, 1998, 2001-2022 Free Software Foundation,
  Inc.

  This file is part of GNU Emacs.

  GNU Emacs is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  GNU Emacs is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

Please read the file COPYING and then do give copies of GNU Emacs to
your friends.  Help stamp out software obstructionism ("ownership") by
using, writing, and sharing free software!
```

I've created several text cheatsheets in the past relating to other
areas of Emacs usage (e.g. `org-mode`). I'll only focus here on the
"core commands" to get going with Emacs.

I'll admit that I'm missing a few facilities provided by the `vi`
family of text editors, but I'm going to wait and try editing with
these core commands first before trying to reproduce `vi` behaviour.

[Next post - Emacs basic help]({{< ref "/serial/relearning_cs/emacs_help" >}})
