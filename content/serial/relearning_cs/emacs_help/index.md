---
title: "(re)learning CS - Emacs help"
date:  2023-04-07 16:00:00 +0200
---

[Previous post - Primer on Emacs]({{< ref "/serial/relearning_cs/emacs_primer" >}})

## Emacs - a self documenting text editor

### Dependencies

On Debian, you may want to install a few extra packages:
* `emacs-common-non-dfsg`
* `info`

Package ending in *-doc usually contain `info` documentation
(e.g. `gdb-doc`).

### Practice

You want to go through Emacs' tutorial: `C-h t`.

You want to peruse the Emacs manual: `C-h r`.

You want to go through the `info` tutorial: `C-h i` then `h`.

### Cheatsheet

```
* GENERAL HELP
--------------

   C-h ?    Help about help commands
   C-h c    Brief help about command char or sequence
   C-h k    Help about command char or sequence
   C-h x    Describe a command.  You type in the name of the
		command.
   C-h a	Command Apropos.  Type in a keyword and Emacs will list
		all the commands whose names contain that keyword.
		These commands can all be invoked with META-x.
		For some commands, Command Apropos will also list a
		sequence of one or more characters which runs the same
		command.
   C-h i	Read included Manuals (a.k.a. Info).  This command puts
		you into a special buffer called "*info*" where you
		can read manuals for the packages installed on your system.
		Type m emacs <Return> to read the Emacs manual.
		If you have never before used Info, type h and Emacs
		will take you on a guided tour of Info mode facilities.
		Once you are through with this tutorial, you should
		consult the Emacs Info manual as your primary documentation.
   C-h m    Read documentation on the current mode
   C-h f    Help about the function

* DOCUMENTATION
---------------

   info-lookup-symbol    Displays the definition of the symbol found 
	    in the manual
   C-h r                 Open the Emacs manual in the info browser
   C-h F                 Find documentation for command


* CODE DOCS
-----------

   C-h S    Symbol help mode
   M-.      Find definition (after generating tags)
```

[Next post - Basic of C with Emacs]({{< ref "/serial/relearning_cs/emacs_c" >}})
