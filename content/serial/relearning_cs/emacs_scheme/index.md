---
title: "(re)learning CS - Scheming with Emacs"
date:  2023-04-21 15:05:00 +0200
---

[Previous post - Managing Emacs packages]({{< ref "/serial/relearning_cs/emacs_packages" >}})

## Why would you Scheme using Emacs?

Scheme is one of many dialect of the Lisp programming language (i.e. a
blending of Lisp and Algol). MIT CS program inspired book
[SICP](https://en.wikipedia.org/wiki/Structure_and_Interpretation_of_Computer_Programs)
introduces students to CS through Scheme. Emacs being a text editor
powered by another flavour of Lisp (i.e. Elisp, for Emacs Lisp), it
makes it a particuliarly suited IDE for playing around with Scheme.

## Dependencies

On Debian:

* mit-scheme
* mit-scheme-doc

Emacs packages:

* geiser-mit

## Demo

Open a scheme file `.scm` in Emacs and run the Scheme REPL
(read-eval-print loop) (a.k.a. interpreter) with `M-x run-geiser`.

Then in another window, write the following *expression*.

```scheme
111
```

Place the cursor at the end of it and press `C-M-x`. This will
*evaluate* the *expression* and print the result to Emacs' bottom
window, in this case `111`.

Try with the following. Think about *evaluating* each *expression* in
order.

```scheme
(define three 3)
(* 3 three)
```

[Next post - Magic with Magit]({{< ref "/serial/relearning_cs/emacs_magit" >}})
