---
title: "2023-04-23 Status Update - (re)learning CS"
date:  2023-04-23 14:00:00 +0200
---

## CS

### C

I've managed to get back into the groove of writing C code. I have
enough program ideas to practice some more. I just need to commit some
time to it.

### Reading [SICP](https://en.wikipedia.org/wiki/Structure_and_Interpretation_of_Computer_Programs)

This CS introductory book focuses on "the fun" of programming, and so
far I would say that it is correct. Scheme does not burden the
programer with the eldritch requirements of building and running a
program: write your code in a syntax-light language and let the REPL
do the rest. Focus on the program itself, not the logistics around it
(e.g. `Makefile` or compilers).

## Emacs

### The good

Vanilla Emacs offers a lot of facilities without diving into
configuration files. The package management is smooth. Using `info`
and reading documentation is suprisingly pleasant on top of being
informative.

Functional programming with an integrated REPL-interpreter is a
breeze.

### The bad

The raw text editing powers provided by `vim` in `normal mode` is yet
unmatched by my current usage of Emacs. I am holding myself in check
here and purposefully do things the way I've learned from Emacs, for
practice' sake. But eventually I'll have to find a way to duplicate
lines or mark paragraphs with a few key presses, among other things.

I haven't even started looking at language servers and
auto-completion.

## Writing

Am I writing as often as I'd like? Not really. But I am definitely
writing more often than I was (if one looks at the frequency of my
posting on this blog this past year). So that is going rather well,
all things considered. Keep in mind that I need to achieve some form
of progress with the projects that feed the writing, in order to come
up with something worthwhile to write.