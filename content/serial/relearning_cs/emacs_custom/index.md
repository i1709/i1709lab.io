---
title: "(re)learning CS - Customizing Emacs"
date:  2023-04-24 21:00:00 +0200
---

[Previous post - Magic with Magit]({{< ref "/serial/relearning_cs/emacs_magit" >}})

## Customizing Emacs

There. Is. So. Much. Potential.

And so many resources out there to get started (maybe too many?). Even
decades old one may contain useful tips.

Here's what I've been using so far:

```elisp
(setq-default c-basic-offset 4)
(setq-default indent-tabs-mode nil)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(setq make-backup-files nil)
(ido-mode 1)
(show-paren-mode 1)
(eldoc-mode 1)
(define-key emacs-lisp-mode-map
  (kbd "M-.") 'find-function-at-point)

;; package management (with MELPA)
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
```

It's a nice beginner friendly default. I worked my way towards it by
reading [this](https://david.rothlis.net/emacs/howtolearn.html) ten
year old guide.

Loaded packages, font and theme packages I leave to Emacs' "Easy
Custom" to take care of. For now.

More links:
* [Emacs Wiki](https://www.emacswiki.org/emacs/BooksAboutEmacs)
* [This StackExchange post](https://emacs.stackexchange.com/questions/342/what-are-the-best-resources-to-learn-emacs)

[Next post - A little recursion]({{< ref "/serial/relearning_cs/a_little_recursion" >}})
