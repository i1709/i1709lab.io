---
title: "About this blog"
date: 2020-09-27 21:00:00 +0200
---

I write about things I want to learn and share.

English is the main language. French and Swedish translations are done
separately.
