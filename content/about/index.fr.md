---
title: "À propos de ce blog"
date: 2020-09-27 21:00:00 +0200
lang: fr
---

J'écris à propos de sujets que je souhaite apprendre et partager.

L'anglais est la langue principale. Les traductions françaises et suédoises
sont faites séparément.
