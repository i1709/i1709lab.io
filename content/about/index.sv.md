---
title: "Om denna blogg"
date: 2020-09-27 21:00:00 +0200
lang: sv
---

Jag skriver om ämnen jag vill lära mig och dela.

Huvudspråket är engelska. Franska och Svenska översättningar görs separat.
