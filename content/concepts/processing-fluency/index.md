---
title: "Processing fluency"
date: 2022-01-18T19:00:00+01:00
categories: ["concepts"]
---

a.k.a. **Cognitive ease and strain**

> Processing fluency is the ease with which information is processed.

Clear, concise and easy to process information is more easily absorbed by the
human brain. Feeling of beauty and pleasure can be associated with the
experience. The [illusory truth effect]({{< ref "/concepts/illusory-truth-effect/index.md" >}})
is a consequence of processing fluency combined with incorrect information.

Conversely, impaired processing fluency (i.e. **cognitive strain**) are
associated with feelings of displeasure and triggers an analytical mode of
thinking.

This can be observed in [evaluation bias related to the quality of handwriting.](https://journals.sagepub.com/doi/10.1177/1948550610368434)
