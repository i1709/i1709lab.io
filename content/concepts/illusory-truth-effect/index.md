---
title: "Illusory Truth Effect"
date: 2022-01-17T18:34:40+01:00
categories: ["concepts"]
---

a.k.a. **illusion of truth effect**, **validity effect**, **truth effect**, or
the **reiteration effect**

> [...] the tendency to believe false information to be correct after repeated
exposure.

[Explanation video by Veritasium.](https://www.youtube.com/watch?v=cebFWOlx848)

Caused by cognitive ease, an easily digested message repeated often enough will
be internalised as true.
