---
title: "Todo"
date: 2022-01-22T20:30:33+01:00
draft: true
---

# Post ideas

* Zettelkasten
* Cooking
  - Learnings about making your own sourdough
* Book Reviews
* CS
  - Unix inter-process communication
  - Difference between a heap and a stack
  - How does a web browser work?
  - Containers
    - debian container image from scratch
    - running `hugo` in a container with `podman`
* Software Development
  - Accelerate / DevOps / 12-Factor App
* Economy
  - Loans and interest rates
  - Savings and investments
* Climate
  - Carbon-neutral energy production & storage
* Words
  - Solastalgia
  - Verisimilitude
  - Diegetic
  - Apophenia
* History
  - Nordic mythology
    - The Myth
    - Its origins
    - What we do know
  - Sweden
    - Territory's History
    - State's Origins
    - Government's History
  - France
    - Territory's History
    - State's Origins
    - Government's History
  
# Other

* Images
  - Create a favicon
