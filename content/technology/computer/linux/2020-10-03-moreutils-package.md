---
title:  "Useful Debian package: moreutils"
date:   2020-10-03 19:00:00 +0200
---

Here comes a quick dump of usage tips for useful utilities found in the
`moreutils` package.

# The Dump

## chronic (runs a command quietly unless it fails)
```bash
chronic rsync -av ~/Documents/ ~/Backup
chronic sh /home/frodo/scripts/email_bkp.sh
```

## combine (line in two files using boolean operations)
```bash
combine file1 and file2
```

## errno (lookup errno names and description)
```bash
errno eintr
errno 4
```

## ifdata (get network interface info w/o parsing ifconfig output)
```bash
ifdata -pa br0
ifdata
```

## ifne (run a program if the standard input is not empty)
```bash
find . -mtime -7 | ifne echo "Found something"
```

## isutf8 (check if a file or standard input is utf-8
```bash
isutf8 file.txt && echo "Yes, it is UTF8"
```

## lckdo (execute a program with a lock held)
```bash
lckdo mylock some-script & lckdo -w mylock echo "Done"
```

## mispipe (pipe two commands, returning the exit status of the first)
```bash
mispipe "echo hi" "false"
```

## parallel (run multiple jobs at once)
```bash
parallel -- "echo one" "echo two" "echo three"
```

## pee (tee standard input to pipes)
```bash
cat file | pee "sort -u > sorted" "sort -R > unsorted"
```
## bash doesn't need pee
```bash
cat file| tee >(sort -u > sorted) >(sort -R > unsorted)
```

## sponge (soak up standard input and write to a file)
```bash
sort file | sponge file
```
## because below you will end up with an empty file
```bash
sort file > file
```

## ts (timestamp standard input)
```bash
tail -f /var/log/mylog | ts > timestamped-log.txt
```

## vidir (edit a directory in your text editor)
```bash
vidir ~/
vidir .
```

## vipe (insert a text editor into a pipe)
```bash
sort file | vipe | head
```

## zrun (automatically uncompress arguments to command)
```bash
zrun sort file.gz
```
