---
title:  "Linux - Magic SysRq recovery"
date:   2020-09-29 21:00:00 +0200
---
# Magic what now?

Excerpt from Wikipedia: 
> In Linux, the kernel can be configured to provide functions for system
debugging and crash recovery. This use is known as the "magic SysRq key". 

The `SysRq` key is also more commonly known and identified as the `Print Screen`
or `Prt Scr`.

In short, if your OS freezes, you can still interact with the kernel. You can
either reboot without running the risk of corrupting your filesystem `OR` call
`oom_kill` to kill the process eating all the available memory.

# Instructions

Press and _hold down_ `Alt + Print Screen` and press the appropriate keys in
sequence, a few seconds apart.

Key sequence to summon `oom_kill`: `f`

Key sequence to reboot safely: `reisub`
