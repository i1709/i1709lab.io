---
title: "tmux - My favorite terminal multiplexer"
date: 2023-01-06T18:00:00+01:00
---

# `tmux`

It allows you to run tty processes inside it, and will preserve them even if
you close your terminal window. On top of that it comes with useful
windowing and scrolling features.

## Configuration example

```nginx
# for neovim
set-option -sg escape-time 10

# for vim
set -g default-terminal "screen-256color"

# for powerline
source "/usr/share/powerline/bindings/tmux/powerline.conf"

# keybindings
## Ctrl+b shortcut for reloading tmux config
bind B source-file ~/.tmux.conf

#sessions
## first session
new -d -s First -n prime

## monitoring session
new -d -s Monitoring -n ht0p "exec htop"
neww -d -n glances "exec glances"

## workspace session
new -s Workspaces -n edit -c ~/ws/
neww -d -n ws1 -c ~/ws/

## privileged session
new -s Privileged -n r00t "exec su -l"
neww -d -n bash
```

## CLI Commands

Run `tmux`:

```bash
tmux
```

Attach to latest session:

```bash
tmux attach
tmux a
```

List sessions:

```bash
tmux ls
```

Create a session:

```bash
tmux new -s sessname
```

Attach to specific session:

```bash
tmux a -t sessname
```

## Keyboard shortcuts

|Shortcut     |Action               |
|-------------|--------------------:|
|C-b "        |split h              |
|C-b %        |split v              |
|C-b arrow key|switch               |
|C-b C-arrow  |resize pane          |
|C-b c        |new window           |
|C-b n        |next window          |
|C-b p        |previous window      |
|C-b ,        |rename window        |
|C-b w        |list window          |
|C-b pgup/pgdn|scrolling            |
|C-b [        |copy mode            |
|C-b x        |kill pane            |
|C-b s        |list sessions        |
|C-b d        |detach session       |
|C-b o        |toggle between panes |
|C-b q        |show pane numbers    |
|C-b t        |show time in pane    |
|C-b ]        |paste                |
|C-b space    |switch to next layout|

## In copy mode

|Shortcut    |Action|
|------------|-----:|
|Ctrl + space|select|
|alt + w     |copy  |

## Bind panes

```bash
tmux set-option -t tmux synchronize-panes on
```
