---
title:  "Linux - Setting up an nginx reverse proxy"
date:   2020-10-06 19:00:00 +0200
---

Let's setup a reverse proxy on Debian with nginx, using `nginx` server.

We will work on a single machine, using the `loopback` interface (i.e. `127.0.0.1`).
Pre-requisites:
 * an HTTP server active and listening (e.g. `127.0.0.1:8080`)
 * an entry in your `hosts` file redirecting a domain name of your choice to your `loopback` interface.

In our example, we will use `example.com`, and the `hosts` file entry will look
like this:
```
127.0.0.1 example.com
```

Let's install it:
```bash
sudo apt install nginx
```

By default, you will find `nginx`'s configuration at `/etc/nginx/`.

Deactivate the default configuration, by first removing the symlink to the
default configuration then restart the server:
```bash
unlink /etc/nginx/sites-enabled/default
service nginx restart
```

You can signal the server to reload it's configuration by sending a `SIGHUP`:
```bash
nginx -s reload
```

Create a reverse proxy configuration file at `/etc/nginx/sites-available/myproxy.conf`:
```nginx
server {
  listen 80;
  server_name www.example.com

  location / {
    proxy_pass http://127.0.0.1:8080;
  }

}
```

Create a symlink to the `sites-enabled` folder:
```bash
link /etc/nginx/sites-available/myproxy.conf /etc/nginx/sites-enabled/myproxy.conf
```

The previous configuration produces the following:
 * `nginx` server listens on port 80 (i.e. HTTP)
 * accepts the following url `http://www.example.com/`
 * redirects requests to a webserver listening on port `8080` at `127.0.0.1`

Let's verify the configuration's integrity:
```bash
nginx -t
```

*In case of error, read it and fix it.*

Now reload the server:
```bash
nginx -s reload
```

Test it.
