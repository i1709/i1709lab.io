---
title:  "Linux - How to setup a virtual network interface with libvirt"
date:   2020-10-02 19:00:00 +0200
---

First, create an `xml` file containing the description of your network interface
and the network (e.g. 10.0.0.0/24) it will manage:

```xml
<network>
  <name>vmbr0</name>
  <bridge name="vmbr0"/>
  <forward/>
  <ip address="10.0.0.1" netmask="255.255.255.0">
    <dhcp>
      <range start="10.0.0.193" end="10.0.0.254"/>
    </dhcp>
  </ip>
</network>
```

Notice we configured a `dhcp`. This will the host to act as a DHCP server on
that interface and provide leases on the range of address provided.

If you are wondering about identifiers (i.e. `uuid`) and MAC addresses, you can
leave that up to `libvirt`.

Then place this file `vmbr0.xml` in `libvirt`'s folder containing qemu networks.
In Debian you would find it by default at `/etc/libvirt/qemu/networks/`.

```bash
cp vmbr0.xml /etc/libvirt/qemu/networks/
```

Define your virtual network interface:

```bash
cd /etc/libvirt/qemu/networks/
virsh net-define vmbr0.xml
```

You can now edit it further if you wish. In the future, be careful to only use
this command to edit it:

```bash
virsh net-edit vmbr0
```

Start it:

```bash
virsh net-start vmbr0
```

To have it starting automatically at boot:

```bash
virsh net-autostart vmbr0
```

Congratulations, you now have a virtual network and an interface that you can
connect your virtual machines to!
