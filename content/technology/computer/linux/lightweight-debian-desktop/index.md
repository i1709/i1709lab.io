---
title: "Lightweight Debian Desktop"
date: 2022-12-19T21:00:00+01:00
---

# Workstation Setup

This setup is geared toward using a lightweight Desktop using AwesomeWM and SLiM.

The following document will refer to your user as `frodo`.

## Debian installer

Use a non-free installer if you need wifi.

Use `dd` (or a `dd`-like way) to create boot USB stick:

```bash
sudo dd bs=4M if=debian-10.1.0-amd64-netinst.iso of=/dev/sdc status=progress oflag=sync
```

WARNING: `UNetbootin` generated boot drives are not 100% reliable.

### Partition

LVM Crypted, an entire disk if possible, keep everything in the same partition.

NOTE: You may want to future proof your `/boot` partition with _at least_ `500M` of disk space.

### Sources

Add contrib and non-free.

### Initial packages

Do not install any available DE if you want a lightweight setup.

Use `xfce` if you need more ease-of-use.

WARNING: Beware `sddm` and `keychain` interaction.

## Manual Setup

Either do the following as `root` or use `su -l`, as sudo can be initially absent.

### Setup WiFi on CLI

Using `wlp99s0` as your wlan interface name:

```bash
ip a
ip link set wlp99s0 up
```

Scan and find the correct ESSID for your wireless LAN:

```bash
iwlist scan | grep ESSID
```

Then edit `/etc/network/interfaces`:

```
# wifi
allow-hotplug wlp99s0
iface wlp99s0 inet dhcp
	wpa-ssid ESSID
	wpa-psk PASSWORD
```

WARNING: The password _will be_ readable!

### Install Packages

#### sudo

To install `sudo` and add `frodo` as a sudoer.

```bash
apt install sudo
usermod -aG sudo frodo
```

You will need to log in again for the changes to be taken into account.

#### Packages for working WM

Install the following packages to have a working WM:

```bash
apt install awesome awesome-extra arandr xserver-xorg slim
```

#### Useful Packages

```bash
apt install cifs-utils tmux bridge-utils haveged htop i3lock qemu-kvm \
  virt-manager moreutils ncdu netcat-traditional nmap ntp rsync pavucontrol \
  pidgin pidgin-sipe pinentry-gnome3 pinentry-curses powerline tcpdump \
  ssh-askpass apg strace terminator xautolock aptitude build-essential
```

#### Firmware Packages

You can try to figure out which firmware packages are required by:

 1. Print firmware related warning

```bash
dmesg | grep -i firmware
```

 2. Search for correct package with apt

```
apt search <insert firmware name or other related string from dmesg print>
```

Usually the following will suffice for a workstation without special graphical
acceleration required:

```bash
apt install firmware-misc-nonfree firmware-realtek
```

Make sure you also have `intel-microcode` package if you are running on an
Intel CPU:

```bash
apt install intel-microcode
```

Same for `amd64-microcode`.

#### Editor Packages

```bash
apt install vim neovim shellcheck
```

#### Dev VM Packages

```bash
apt install packer vagrant vagrant-libvirt
```

#### Pretty Packages

```bash
apt install plymouth plymouth-themes
```

#### Ergonomy Packages

```bash
apt install redshift
```

#### Security Packages

```bash
apt install libpam-tmpdir
```

##### Configure `plymouth`

Edit `/etc/initramfs-tools/modules` (e.g. for intel):

```
# KMS
intel_agp
drm
i915 modeset=1
```

Edit `/etc/default/grub` (e.g for netbooks):

```
GRUB_GFXMODE=1024x600x32
```

In the same file, search and edit:

```
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
```

Update `grub`:

```bash
update-grub
```

As `root`:

  * list themes

```bash
plymouth-set-default-theme -l
```

  * set theme

```bash
plymouth-set-default-theme -R THEME
```

If error, `firmware-linux-nonfree` might be needed.

### Configuration

#### WM working

In `~/.bash_profile`:

```bash
source ~/.profile
if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
    exec startx
fi
```

In `~/.bashrc`:

```bash
if [ -f "$(which powerline-daemon)" ]; then
    powerline-daemon -q
    # shellcheck disable=SC2034
    POWERLINE_BASH_CONTINUATION=1
    # shellcheck disable=SC2034
    POWERLINE_BASH_SELECT=1
    . /usr/share/powerline/bindings/bash/powerline.sh
fi
```

In `~/.xinitrc`:

```bash
exec $1
```

If you have several WMs and you want SLiM to pre-select one in particular, use the following in your `~/.xsession`:

```
XSESSION="awesome"
```

### Virtualize with `virt-manager` and `qemu-kvm`

Install `qemu-kvm` & `virt-manager`:

```bash
apt install qemu-kvm virt-manager
```

To use virt-manager as `frodo`:

```bash
usermod -aG libvirt frodo
```

### Snoop the network with `wireshark`

Install `wireshark`:

```bash
apt install wireshark
```

Answer positively to the question to use it as `frodo`, then:

```bash
usermod -aG wireshark frodo
```

### Version Debian configuration with `etckeeper`

Install `etckeeper`:

```bash
apt install etckeeper
```

Uncomment the following:

```
AVOID_DAILY_AUTOCOMMITS=1
AVOID_COMMIT_BEFORE_INSTALL=1
```

WARNING: The cron script might be not be taking into account `AVOID_DAILY_AUTOCOMMITS` setting, you may have to fix it.

### Run minimal security audits with `chkrootkit` and `lynis`

Install `chkrootkit` and `lynis`:

```bash
apt install chkrootkit lynis
```

Run them:

```bash
chkrootkit
lynis -c
```
