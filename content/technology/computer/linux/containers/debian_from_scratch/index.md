---
title: "Debian container from scratch"
date: 2023-01-16T17:30:00+01:00
---

Let's create a Debian container from scratch while learning more about the
tools and programs used along the way!

**NB**: This will be _very_ rootfull. So proceed, as `root`, with **caution**.

# Raw instructions

```bash
newcontainer=$(buildah from scratch)

scratchmnt=$(buildah mount $newcontainer)

apt install debootstrap

debootstrap bullseye $scratchmnt

buildah umount $newcontainer

buildah commit $newcontainer debian-bullseye:latest

podman run localhost/debian-bullseye /usr/bin/cat /etc/os-release
```

Save image to be used by a non-privileged user:
```bash
podman save --output /tmp/bullseye.tar localhost/debian-bullseye
```

Load image as a non-privileged user (i.e. drop `root` privileges for this):
```bash
podman load --input /tmp/bullseye.tar
```

# Step-by-step

Create an empty container and assign its `id` to a shell environment variable:
```bash
newcontainer=$(buildah from scratch)
```

You should be able to list it with:
```bash
buildah containers
```

Mount the new empty container and assign its location to a shell environment
variable:
```bash
scratchmnt=$(buildah mount $newcontainer)
```

Install Debian's `debootstrap` package:
```bash
apt install debootstrap
```

Use `debootstrap` to create a minimal `bullseye` system into the container's
mounted location:
```bash
debootstrap bullseye $scratchmnt
```

Unmount the container:
```bash
buildah umount $newcontainer
```

Create (i.e. `commit`) an image from the container:
```bash
buildah commit $newcontainer debian-bullseye:latest
```

You should be able to list it with:
```bash
buildah images
```

Test run your container image:
```bash
podman run localhost/debian-bullseye /usr/bin/cat /etc/os-release
```

# Concepts

## Container

A running copy of the Linux kernel (i.e. `cgroups` and `namespaces` with
`capabilities`) packaged as a file-system. An OS within an OS.

Modern container technique layers this file-system into different reusable
layers that are read-only, with the top layer writable.

# Tools

A container is only a file-system containing a Linux kernel system
specific files. There are many ways to create them, depending on whether
you want to do it manually and which distribution you are containerizing.

We will use the following tools:
* `podman`
* `buildah`
* `debootstrap`

## podman

[Straight from the homepage](https://podman.io/):

> Podman is a daemonless container engine for developing, managing, and
  running OCI Containers on your Linux System

There are a few things to unpack here. The important part is `daemonless`:
`podman` is an alternative to `docker`, which runs as a system daemon and
requires `root` privileges to manage containers. Docker's rootfull nature
made a lot a people hesitate to use it and `podman` was developed as an
answer.

## buildah

[Official description](https://buildah.io/):

> A tool that facilitates building OCI container images.

It can do [much more](https://github.com/containers/buildah) than described
above.

## debootstrap

[From Debian's wiki](https://wiki.debian.org/Debootstrap):

> debootstrap is a tool which will install a Debian base system into a
  subdirectory of another, already installed system.

It can be used to _bootstrap_, as an example, an empty file-system mounted on a
host with the tool installed. Let's say: create a virtual machine disk file
(i.e. `qcow` format), partition and mount it on a `loop` device on your system
then use `debootstrap` to populate it with everything necessary for it to
function as a Debian distribution.

# Sources

[Buildah: Create images from Scratch](https://www.server-world.info/en/note?os=Debian_11&p=buildah&f=2)
