---
title:  "Linux - Using letsencrypt with nginx reverse proxy"
date:   2020-10-09 17:00:00 +0200
---

If your webserver is exposed to the internet, it means two things:
 * you have a domain name with records pointing to your webserver
 * you need to secure access to it over HTTPS


We are going to use `letsencrypt` to secure our connections to webservers
accessed through an `nginx` reverse proxy.

To learn more about simply setting up a reverse proxy with `nginx`, read [this post.]({{< ref "./2020-10-06-nginx-reverse-proxy" >}})

You will need `root` privileges.

Let's assume the following:
 * your domain name is `example.com`
 * your webserver's public address can be resolved at `www.example.com`
 * your webserver is running on a server connected to the reverse proxy over a
   private network at 192.168.0.1/24 on port 80


Install `certbot`:
```bash
apt install certbot
```

Run it for `nginx`:
```bash
certbot --nginx
```

After answering the questions asked by `certbot`, if everything is in order,
a pair of certificates will be provided to your host running `nginx`.

By default, you can find them here: `/etc/letsencrypt/live/www.example.com/`.

Now let's configure `nginx`:
```nginx
ssl_certificate /etc/letsencrypt/live/www.example.com/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/www.example.com/privkey.pem;
server_tokens off;

server {
  listen 80;

  server_name *.example.com;

  rewrite ^ https://$host$request_uri? permanent;
}

server {
  listen 443 ssl;
  server_name www.example.com;
  ssl on;

  location / {
    proxy_pass http://192.168.0.1:80;
  }

}

```

The previous configuration does multiple things, on top of indicating where
your public and private key pair can be found.

*Firstly*, your `nginx` server will not provide any information about itself (e.g.
version) with `server_tokens` turned `off`.

*Secondly*, all HTTP request towards your domain will be automatically rewritten to
HTTPS ones.

*Finally*, you will connect to your webserver via the reverse proxy, and the
connection between your client and the reverse proxy will be done over HTTPS.

Let's verify the configuration's integrity:
```bash
nginx -t
```

*In case of error, read it and fix it.*

Now reload the server:
```bash
nginx -s reload
```

Test it.
