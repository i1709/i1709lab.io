---
title:  "Linux - A dump of my network troubleshooting tips"
date:   2020-10-01 18:00:00 +0200
---
# Debugging the network on Debian

Throughout this document, we will consider the interface to debug as `enp1s0`.

Administrative privileges will be necessary.

## Use `tcpdump` for packet capture

To listen on `enp1s0`:

```bash
tcpdump -ni enp1s0 -s0
```

Only print packets involving a specific `host`:

```bash
tcpdump -ni enp1s0 -s0 host 192.168.0.1
```

Write packet capture to file `/tmp/dump.pcap`:

```bash
tcpdump -ni enp1s0 -s0 -w /tmp/dump.pcap
```

Buffer output to file `/tmp/dump.pcap` while viewing it on `stdout`:

```bash
tcpdump -ni enp1s0 -s0 -l | tee /tmp/dump.pcap
```

Print link layer address of captured packets for ports 67 or 68:

```bash
tcpdump -ni enp1s0 port 67 or port 68 -e
```

## Use `iptables` to inspect / modify linux routing / firewalling rules

### Lookup rules and stats

To lookup all the rules:

```bash
iptables -L -v -n
```

To lookup the rules of a specific `chain` (e.g. `INPUT`):

```bash
iptables -L INPUT -v -n
```

### Save rules

```bash
iptables-save -f ~/iptables.dump
```

### Restore saved rules

```bash
iptables-restore ~/iptables.dump
```

### Delete all existing rules

**Do this only if you have a way of redeploying them (e.g. `reboot`,
iptables-save & iptables-restore)**

```bash
iptables -F
iptables -t nat -F
iptables -t mangle -F
iptables -X
```

### List rules

#### In filter table (i.e. default)

```bash
iptables -S
iptables -t filter -S
```

#### In nat table

```bash
iptables -t nat -S
```

### Delete rules by specification

It helps to list them first, remember to not copy the `-A`/`-I` part though.

#### From filter table

```bash
iptables -D <rule>
```

#### From nat table

```bash
iptables -t nat -D <rule>
```

#### E.g.

```bash
iptables -S
```

Outputs:

```
-A INPUT -m conntrack --ctstate INVALID -j DROP
```

Then delete that with:

```bash
iptables -D INPUT -m conntrack --ctstate INVAlID -j DROP
```

### Delete by chain and number

```bash
iptables -L --line-numbers
iptables -D <CHAIN> <num>
```

## Use `ss` to inspect sockets

List all (listening and non-listening) in TCP with associated process number:

```bash
ss -pat
```

In UDP:

```bash
ss -pau
```

List listening sockets in TCP without resolving service namesi with associated
process number:

```bash
ss -lntp
```

## Use `netstat` to figure out if packets are dropped

```bash
netstat -i
```

## Use `ethtool` to print interface statistics

```bash
ethtool -S enp1s0
```
