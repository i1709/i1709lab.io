---
title:  "Git - User configuration"
date:   2021-01-08 18:00:00 +0100
---

# Configuration

## User configuration

You can configure `git`'s configuration for you user this way:
```bash
git config --global user.name "John Doe"
git config --global user.email john@doe.earth
```

The `--global` option signifies that all repositories handled by the user will
use this configuration. In a `unix` type OS, you will find your user's
configuration file in your `$HOME` folder (i.e. `~/.gitconfig`).

Here is a sample `.gitconfig` file:

```ini
[user]
        name = John Doe
        email = john@doe.earth
[core]
        editor = nvim
[diff]
        tool = vimdiff
[alias]
        st = status
        cmt = commit
        dh = diff HEAD
[push]
        default = current
[pull]
        rebase = false
[rerere]
        enabled = true
```

## Local configuration

If you use the `--local` (i.e. the default) option instead, the configuration
will only apply to the repository you are working in. The file will be within
the repo's configuration folder (i.e. `.git/config`).

## System configuration

Using the `--system` option will apply the configuration for all users of the
OS you are using. It will be found among system files (i.e. `/etc/gitconfig`
for a `unix` type OS).
