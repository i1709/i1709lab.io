---
title:  "Git - Setup a server"
date:   2021-01-05 17:00:00 +0100
---

# Setup a `git` server

## Requirements

Have `root` privileges and `git` installed on the server. Your local user should
have remote `ssh` access to the server. A useful tool to have is `rsync`.

## Instructions

On the server, as `root` or with `sudo` privileges, create a `git` specific
user:

```bash
adduser gituser
```

Then add ssh public keys of remote users who will be allowed to use `gituser`:

```bash
cd
mkdir .ssh && chmod 700 .ssh
touch .ssh/authorized_keys && chmod 600 .ssh/authorized_keys
chown gituser:gituser .ssh -R
cat pub.key >> authorized.keys
```

Create a folder for git repositories:

```bash
cd /opt/git
chown gituser:gituser /opt/git
```

Install and setup `git-shell`:

```bash
which git-shell >> /etc/shells
chsh gituser -s $(which git-shell)
cp /usr/share/doc/git-<version-number>/contrib/git-shell-commands /home/gituser -R
chown gituser:gituser /home/gituser/git-shell-commands/ -R
chmod +x /home/gituser/git-shell-commands/help
chmod +x /home/gituser/git-shell-commands/list
```

## Create new project

Create a new project on the client and copy it to the server:

```bash
cd project
git init
git add .
git commit
cd ..
git clone --bare project project.git
rsync -av project.git myserver:/tmp/
```

Copy and change permissions of bare project folder:

```bash
cp -r /tmp/project.git /opt/git/
chown -R gituser:gituser /opt/git/project.git
```

Clone the project from the server to the client:

```bash
git clone gituser@myserver:/opt/git/project.git
```

Congratulations!
You now have a working git server to host your repositories.
