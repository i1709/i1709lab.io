---
title:  "Git - Overview of a classic workflow"
date:   2021-01-06 09:00:00 +0100
---

# Overview of a typical `git` workflow

Here I will describe the major branch handling steps.

## Clone repo

```bash
git clone <url>
```

## Create branch and switch to it in one command

```bash
git checkout -b <branch>
```

Keep in mind that the branch will be based off the current branch you type this
command from.

## Push branch to a specific remote

```bash
git push -u origin <branch>
```

If you cloned your repository from a remote location, you branch's remote should
default to the correct remote repository. A simple `push` is enough.

```bash
git push
```

If the branch is locally created, `git` will prompt you with the appropriate
command to create the `upstream` (i.e. remote) branch first.

## Rebase branch based on remote

```bash
git pull --rebase origin <master branch>
```

## Push after rebasing

```bash
git push --force-with-lease
```

Be aware of the risks involved in rebasing a shared branch!

## Delete branch locally

```bash
git branch -D <branch>
```

## Delete branch locally and remotely

```bash
git push origin --delete <branch>
```

## Prune stale references

```bash
git remote prune origin
```

## Cherry pick a specific commit from another branch

```bash
git cherry-pick <commit-hash>
```
