---
title:  "Git - Some extra tips"
date:   2021-01-09 13:00:00 +0100
---

# Extra tips

## `log`

Parameters for better logging:

```bash
git log --oneline --graph
```

Log actual changes in a file:

```bash
git log -p source-file.c
```

Only Log changes for some specific lines in a file:

```bash
git log -L 1,5:source-file.c
```

Log changes not yet merged to the parent branch:

```bash
git log --no-merges master..
```

## `show`

Show a file from another branch:

```bash
git show some-branch:source-file.c
```

## `merge`

Force git to keep branches history during merge:

```bash
git merge --no-ff some-branch-name
```

## `reset`

Un-stage the added files:

```bash
git reset
```

Rollback and **erase** all changes made after pointed commit:

```bash
git reset --hard <commit-hash>
```

Edit, re-stage and re-commit files in a different order:

```bash
git reset <commit-hash>
```

Revert past X commits while keeping the changes (i.e. as un-staged):

```bash
git reset --soft <commit-hash>
```

## `revert`

Revert a commit softly:

```bash
git revert -n
```

## `difftool`

Try to diff the entire folder:

```bash
git difftool -d
```

## `stash`

Stash only some files:

```bash
git stash -p
```

## `bisect`

Find a commit that broke your feature using `bisect`:

```bash
git bisect
git bisect start
git bisect good <commit-hash>
git bisect bad <commit-hash>
git bisect bad
git bisect good
git bisect reset
git bisect log
```

## `rebase`

Change commit message of a commit you haven't pushed yet:

```bash
git rebase -i <commit-hash>
```

## Manual patch

Send uncommitted changes as a patch file and apply them un-staged:

```bash
git diff > file.patch
git apply file.patch
```
