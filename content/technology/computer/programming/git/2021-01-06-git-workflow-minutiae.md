---
title:  "Git - Minutiae of a classic workflow"
date:   2021-01-06 19:00:00 +0100
---

# Minutiae of a typical `git` workflow

Here I will describe the major commit handling steps.

## Stage changes in preparation for a commit

### Per file

```bash
git add source_file.c
```

### All changes from this folder and down recursively

```bash
git add .
```

### Some changes (i.e. interactively)

```bash
git add -p
```

## Stash for later

If you have changes that you neither want to commit nor lose (e.g. you need to
switch branch very urgently) , you can `stash` them for later use.

```bash
git stash .
```

### View stash

```bash
git stash list
```

### Retrieve from stash

```bash
git stash pop
```

### Empty stash

```bash
git stash drop
```

## Commit your changes

### All staged changes

```bash
git commit
```

Same principle for files applies as with staging (see `add`).

### Amend your commit

You realise that you made a small error you want to fix in your previous commit,
without having to `reset` it or make a `commit` on top. You want to `amend` it.

First make your changes, then:

```bash
git commit --amend
```

You also have an opportunity here to modify the `commit` message.

## Send your commits upstream

```bash
git push
```
