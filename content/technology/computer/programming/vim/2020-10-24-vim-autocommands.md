---
title:  "A vim tip: autocommands"
date:   2020-10-24 16:00:00 +0200
---

# Autocommands

You can configure `vim` to execute commands when certain events happen, with
`autocmd`.

E.g.:
```vim
autocmd FileType python setlocal tabstop=8 expandtab shiftwidth=4 softtabstop=4
```

Here we are looking for the `FileType` event to have `python` as pattern, i.e.
open a `python` file in a buffer. The command used in the example is to set
indentation rules.

# Pitfall of autocommands

There is no way for `vim` to know if any `autocmd` you specify are supposed to
replace previous ones. It means that any time you source a file containing
autocommands, they will be duplicated. This might create problems for you.

# The safe way of using autocommands

When usint `autocmd`, group them with `augroup`, and start each grouping with
`autocmd!`.

E.g.:
```vim
augroup filetype_python
  autocmd!
  autocmd FileType python setlocal tabstop=8 expandtab shiftwidth=4 softtabstop=4
augroup END
```

Using named groups with `augroup` allows you to organise your autocommands by
topic, and `autocmd!`, **at the beginning**, will clear the group every time it
is entered, ensuring no duplicates are created (i.e. by default, `augroup`
combines identically named groups).
