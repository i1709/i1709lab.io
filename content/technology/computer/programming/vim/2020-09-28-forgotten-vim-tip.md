---
title:  "A forgotten vim tip: local rc files"
date:   2020-09-28 22:00:00 +0200
---

```bash
echo "Greetings."
```

Since I keep forgetting this, I might as well leave myself a little reminder
here!

It is convenient to use a local vim run control (i.e. rc) file in a project's
root directory, with configuration very specific to that project.

Let's say that your project `~/myproject` contains `yaml` files, and you want 
a very specific indentation rules to be applied, that you do not want to clutter
your regular `.vimrc` file with.

Do as follows:

```bash
cd ~/myproject
touch .vimrc
```

And edit `.vimrc` with the following:

```vim
echom "Hello, this is myproject's local rc file"
echom expand("%:p")
echom "-_-!"

augroup filetype_yaml
    autocmd!
    autocmd FileType yaml setlocal tabstop=8 expandtab shiftwidth=2 softtabstop=2
augroup END
```

If you use `neovim`, name your rc file `.nvimrc` instead.

```bash
echo "Live long and prosper."
```
