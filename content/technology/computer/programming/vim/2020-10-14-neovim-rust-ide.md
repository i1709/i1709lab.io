---
title:  "Neovim - IDE for rust"
date:   2020-10-14 21:00:00 +0200
---

# Rust

## Install

The LSP server for `rust` is `rls`.

On Debian Testing the toolchain returns `stable-x86_64-unknown-linux-gnu`.

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
rustup component add rls rust-analysis rust-src --toolchain stable-x86_64-unknown-linux-gnu
```

**IMPORTANT**: make sure `rls` runs after install

# Neovim

## Plugins

Plugins managed by `vim-plug`

The LSP client used is `ale`.

**NOTE**: `ale` works only inside a `cargo` generated project (i.e. it will
search for `Cargo.toml`)

```vim
Plug 'dense-analysis/ale'
Plug 'rust-lang/rust.vim'
```

## Configuration

This will set various fixers including `rustfmt` and `rls` as the linter.

```vim
let g:rustfmt_autosave = 1
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'rust': ['rustfmt'],
\}
let g:ale_linters = { 'rust': ['rls'], }
```

