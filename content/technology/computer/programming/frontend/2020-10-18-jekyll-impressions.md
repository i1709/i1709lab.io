---
title:  "Web - My impressions on Jekyll"
date:   2020-10-18 14:00:00 +0200
---
# Context

In this post, I wish to share my impressions on `jekyll`. Naturally, they are
coloured by my own needs and requirements, therefore I will start by exposing
those.

## Why this blog

Resources on the web have been a big help in my education and career, and I
wanted to start giving back to the community.

Moreover, I have always wanted to improve my knowledge around web development,
and I am a strong believer in "learning by doing".

## Why a static site

The purpose of this blog does not call for client side processing, as static
pages fulfill all the requirements of transmitting textualised information.

Any form of scripting on this blog should only be geared towards ease of use,
visual comfort and accessibility.

## Personal preferences

I enjoy structuring my texts in `markdown`. It is a rather widespread standard
these days and static site generators' capability to convert `markdown` to HTML
is much appreciated.

# Impressions

## Pros of `jekyll`

What I appreciate about `jekyll`:
 * its ease of use
 * an abundance of online resources


I was able to very quickly get around to generate a deployable static site,
with adequate results. In terms of community support, `jekyll` is very much
out there with a wealth of blogs and videos.

## Cons of `jekyll`

What I found problematic with `jekyll`:
 * `ruby`
 * a rather roundabout way of overriding themes
 * creating, using and maintaining your own themes


My chosen default theme currently serve the purpose of this blog, but my
attempt to modify it became quickly laborious.

I have no wish at this time to dabble with `ruby` more than necessary, and I
have no wish to look too closely into the inner workings of `jekyll` itself to
gain insight into creating a theme for it.

# Last words

I will continue to use `jekyll` to generate this blog, for the time being.

I have given `hugo` a test ride, and I find its approach to theming to be very
much what I have been looking for.
