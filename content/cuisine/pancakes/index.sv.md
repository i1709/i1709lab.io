---
title: "Surdegspannkakor"
date: 2023-01-07T20:00:00+01:00
categories: ["kokonst"]
lang: sv
---

# Kvällen före 

* 100 g av surdegsstarter
* 120 g av vetemjöl
* 2.25 dl av mjölk
* 1 msk socker

Blanda ihop vetemjöl, socker, mjölk och surdegsstarter. Låt stå täckt över
natten i rumstemperatur.

# Morgon efter

* 1 ägg
* 25 g smält smör
* 1/4 tsk salt
* 1 tsk bakpulver

Vispa ihop ägg, smält smör och rör sedan ner smeten från gårdagen. Häll ner
salt och bakpulver och rör om. Hetta upp en stekpanna med lite smör och stek på
medelvärme surdegspannkakorna.
