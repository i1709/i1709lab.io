---
title: "Recette de cookies végan"
date: 2022-02-07T18:00:00+01:00
categories: ["cuisine"]
lang: fr
---

Cuire entre 15 et 20 cookies, en fonction de leur taille.

## Ustentiles

* un four et de quoi faire de la pâtisserie

## Ingrédients

* 2 dL de sucre
* 1 dL de "lait" d'avoine
* ~0.8 dL de beurre de cacahuète
* 2 c.à.s. d'huile de colza
* 1 c.à.c. d'extrait de vanille
* ~2.3 dL de farine de blé complet
* ~2.3 dL de flocons d'avoine
* ½ c.à.c. de bicarbonate de soude
* ½ c.à.c. de sel
* 1 dL d'éclats de chocolats végans
* 1 dL de morceaux de noix

## Préparation

1. préchauffer le four à 220 °C
2. mélanger le sucre, le "lait" d'avoine, le beurre de cacahuète, l'huile de colza et l'extrait de vanille ensemble jusqu'à obtenir un résultat lisse
3. mélanger la farine, l'avoine, le bicarbonate et le sel dans un second bol
4. combiner au mélange de beurre de cacahuète
5. replier les éclats de chocolats et de noix dans la pâte ainsi obtenue
6. déposer les cookies sur la plaque de cuisson préparée
7. cuir au four jusqu'à ce que les cookies commencent à brunir, environ 10 mins.
8. refroidir les cookies sur la plaque environ 10 mins
9. retirer les cookies de la plaque

## Astuces

Si la pâte à l'air trop sèche, ajoutez-y un peu de liquide ou d'huile. Mélangez
à la main si nécessaire, en évitant de réchauffer la pâte.

Dégustez les cookies !
