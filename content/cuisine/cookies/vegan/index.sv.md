---
title: "Veganskt kakrecept"
date: 2022-02-07T18:00:00+01:00
categories: ["kokonst"]
lang: sv
---

Baka mellan 15 och 20 kakor, beroende på storlek.

## Verktyg

* ugn och bakning verktyg

## Ingredienser

* 2 dl socker
* 1 dl havredryck
* ~0.8 dl jordnötssmör
* 2 msk rapsolja
* 1 tsk vaniljextrakt
* ~2.3 dl fullkornsvetemjöl
* ~2.3 dl havregryn
* ½ tsk bikarbonat
* ½ tsk salt
* 1 dl veganska chokladbitar
* 1 dl valnötsbitar 

## Gör så här

1. värm upp ugnen till 220 °C
2. blanda socker, havredryck, jordnötssmör, olja och vanilj slätt
3. blanda mjöl, havre, bikarbonat och salt i en annan skål
4. tillsätt mjölblandningen i jordnötssmör mixen
5. rör ner chokladbitar och valnötsbitar i smeten
6. klicka ut kakorna på bakplåten
7. baka i ugnen tills kakorna börjar bli bruna på kanterna, ~10 mins
8. låt kakorna svalna på bakplåten ~10 mins
9. ta bort kakorna från bakplåten

## Tips

Om smeten blir för torr, lägga till det lite havredryck eller olja. Mixa med
handen om det behövs, utan att värma upp smeten.

Njut av kakorna!
