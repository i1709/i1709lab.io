---
title: "Vegan Cookie Recipe"
date: 2022-02-07T18:00:00+01:00
categories: ["cooking"]
---

Make between 15 and 20 cookies, depending on cookie sizes.

## Tools

* oven and baking equipment

## Ingredients

* 2 dL of sugar
* 1 dL of oat drink
* ~0.8 dL of peanut butter
* 2 tablespoons of canola oil
* 1 teaspoon of pure vanilla extract
* ~2.3 dL of whole wheat flour
* ~2.3 dL of rolled oats
* ½ teaspoon of baking soda
* ½ teaspoon of salt
* 1 dL of vegan chocolate chips
* 1 dL of walnut pieces

## Preparation

1. preheat oven at 220 °C
2. mix sugar, oat drink, peanut butter, canola oil and vanilla extract together
   until completely smooth
3. mix flour, oats, baking soda and salt in a separate bowl
4. add to the peanut butter mixture and stir to combine
5. fold chocolate chips and walnut pieces into the flour mixture
6. form cookies with the batter onto the baking sheet
7. bake cookies in oven until browned along the edges, ~10 mins.
8. cool the cookies on the sheet for ~10 mins
9. remove the cooling rack to cool completely

## Tips

If the batter seems too dry, you can add a bit of liquid and oil. Fold quickly
by hand if necessary, without warming up the batter.

Enjoy the cookies!
