---
title:  "Sauce curry facile"
date:   2021-02-28 11:00:00 +0100
lastmod: 2022-01-23 16:00:00 +0100
categories: ["cuisine"]
lang: fr
---

# Abréviations utilisées dans cette recette

c.à.s. signifie cuillère à soupe

c.à.c. === cuillère à café

# La Recette

## Mise en place

Il est important en cuisine d'effectuer une bonne `mise en place` **avant** de
commencer toute cuisson.

### Ustensiles

* une poêle à sauce

### Ingrédients

* un oignon ou une échalote, finement haché
* 2-3 gousses d'ail, finement haché
* environ 1 c.à.s. de gingembre finement haché
* un piment finement haché (rouge si plat avec protéine, vert sans)
* une conserve de tomates écrasées ~400 g (p. ex. "**Mutti**")
* 1 c.à.s. de poudre de curry (jaune)
* 1 c.à.c. de coriandre, moulue
* optionnel -- 1 c.à.c. de graines de moutarde noire
* optionnel -- 1 ½ c.à.c. de graines de fenugrec
* une demie c.à.c. de cumin, moulue
* huile de cuisson, ~ 1 c.à.s., préférer sans arôme et haute température
  d'ébullition (p. ex. tournesol)
* du sel et du poivre, selon les goûts

## Préparation

1. chauffer l'huile sur la poêle, à feu moyen
2. optionnel -- ajouter les graines de moutardes et de fenugrec, attendre que
les graines de moutardes commencent à "éclater"
3. ajouter les oignons (ne pas laisser jaunir)
4. ajouter l'ail, le gingembre et le piment, laisser cuir ~ une minute
5. optionnel -- au besoin, ajouter un peu d'huile
6. ajouter les épices, cuire ~ 60 s à feu moyen-élevé, mais pas plus, sinon les
  épices vont bruler
7. ajouter les tomates écrasées, laisser mijoter quelques minutes à feu doux
8. goûter la sauce, ajoutez un peu de sel et de poivre, selon les goûts

La sauce est maintenant prête à être ajoutée dans une casserole où cuit déjà
d'autres aliments (p. ex. des lentilles, des haricots ou de la viande).

# Exemple d'utilisation : Curry de haricots rouges

1. cuire 2 pommes de terre et 2 carottes coupées en dés, dans ~ 500 ml d'eau
2. ajouter un bouillon de cube
3. préparer la sauce curry passe-partout
4. lorsque les légumes sont cuits et que la sauce est sur le point d'être
  prête, ajouter une boite de haricots rouge (~400 g) précuites
5. ajouter la sauce
6. ajouter une ou deux c.à.s. de sauce soja ou tamarin, pour ajouter du corps
  au plat
7. sel et poivre, au besoin
8. déguster, s'accompagne facilement avec du riz ou de la semoule
