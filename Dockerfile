FROM localhost/debian-basic

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && \
        apt install -y hugo && \
        rm -rf /var/lib/apt/lists/*

RUN mkdir /iraenix

WORKDIR /iraenix

EXPOSE 1313/tcp

ENTRYPOINT ["hugo"]

CMD ["server", "-D", "--bind=0.0.0.0"]
