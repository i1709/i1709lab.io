# How to use Hugo

Clone theme:

```bash
git clone https://github.com/bake/solar-theme-hugo.git themes
```

Create site:

```bash
hugo new site my-blog
```

Create a post:

```bash
hugo new posts/my-first-post.md
```

Run server with drafts:

```bash
hugo server -D
```

Build for production (i.e. inside `public`): 

```bash
hugo
```

With drafts: 

```bash
hugo -D
```

# Use with `podman`

**NB**: I've created my own image `debian-hugo-iraenix:latest`. Use an image
containing `hugo` with `--bind=0.0.0.0` `CMD` argument.

```bash
podman run --name hugo-iraenix --rm -p 1313:1313 -d -v $(pwd):/iraenix debian-hugo-iraenix:latest
```

# Hugo related improvements

* Experiment with themes
* Figure out a sustainable way to handle images
